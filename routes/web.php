<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {

   return $router->app->version();
});


Route::get('/', function() {
    return 'Welcome to AKUNTA API';
});

Route::group(['prefix' => 'v1', 'namespace' => 'General'], function () {
    Route::get('/', function() {
        return 'Welcome to AKUNTA API';
    });
    Route::post('/login', 'Login@index');
    Route::get('/generate-token', 'Login@generate_token');

});

// acc
Route::group(['prefix' => 'v1', 'namespace' => 'Acc'], function () {

    // COA
    Route::get('/coa', 'Coa@index');
    Route::post('/coa/insert', 'Coa@store');
    Route::post('/coa/detail', 'Coa@detail');
    Route::post('/coa/update', 'Coa@update');
    Route::post('/coa/delete', 'Coa@delete');
    Route::get('/coa-pembayaran', 'Coa@pembayaran');

    // Jurnal Umum
    Route::post('/jurnal-umum', 'JurnalUmum@list');
    Route::get('/jurnal-umum/create', 'JurnalUmum@create');
    Route::post('/jurnal-umum/insert', 'JurnalUmum@store');
    Route::post('/jurnal-umum/edit', 'JurnalUmum@edit');
    Route::post('/jurnal-umum/update', 'JurnalUmum@update');
    Route::post('/jurnal-umum/delete', 'JurnalUmum@delete');

    // Arus Kas
    Route::post('/arus-kas', 'ArusKas@index');

    // Laba Rugi
    Route::post('/laba-rugi', 'LabaRugi@index');

    // Neraca
    Route::post('/neraca', 'Neraca@index');

    // Neraca
    Route::post('/buku-besar', 'BukuBesar@index');

    // Assets
    Route::get('/asset', 'Asset@index');
    Route::get('/asset/create', 'Asset@create');
    Route::post('/asset/insert', 'Asset@store');
    Route::post('/asset/detail', 'Asset@detail');
    Route::post('/asset/delete', 'Asset@delete');

    // Kategori Asset
    Route::get('/asset-kategori', 'AssetKategori@index');
    Route::post('/asset-kategori/insert', 'AssetKategori@store');
    Route::post('/asset-kategori/detail', 'AssetKategori@detail');
    Route::post('/asset-kategori/update', 'AssetKategori@update');
    Route::post('/asset-kategori/delete', 'AssetKategori@delete');

    // Penyusutan Asset
    Route::get('/asset-penyusutan', 'AssetPenyesuaian@cron_penyusutan_asset');

    // Anggaran
    Route::get('/anggaran', 'Anggaran@list');
    Route::post('/anggaran/insert', 'Anggaran@store');
    Route::post('/anggaran/edit', 'Anggaran@edit');
    Route::post('/anggaran/update', 'Anggaran@update');
    Route::post('/anggaran/delete', 'Anggaran@delete');
    Route::post('/anggaran/detail', 'Anggaran@detail');

});
Route::group(['prefix' => 'v1', 'namespace' => 'Masterdata'], function () {
    Route::get('/jenis-produk', 'JenisProduk@index');
    Route::post('/jenis-produk/insert', 'JenisProduk@store');
    Route::post('/jenis-produk/update', 'JenisProduk@update');
    Route::post('/jenis-produk/delete', 'JenisProduk@delete');
    Route::post('/jenis-produk/detail', 'JenisProduk@detail');
    Route::get('/jenis-produk/produk-nya', 'JenisProduk@jenis_dan_produk_nya');
    Route::get('/jenis-produk/count', 'JenisProduk@listCount');

    Route::get('/customer', 'Customer@index');
    Route::post('/customer/insert', 'Customer@store');
    Route::post('/customer/update', 'Customer@update');
    Route::post('/customer/delete', 'Customer@delete');
    Route::post('/customer/detail', 'Customer@detail');
    Route::post('/customer/auth', 'Customer@auth');

    Route::post('/reseller-active', 'Reseller@active');
    Route::post('/reseller-detail', 'Reseller@reseller_by_id');

    Route::post('/province', 'Alamat@province');
    Route::get('/province-search', 'Alamat@province_search');

    Route::post('/city', 'Alamat@city');
    Route::get('/city-search', 'Alamat@city_search');

    Route::post('/subdistrict', 'Alamat@subdistrict');
    Route::get('/sitemap', 'Alamat@sitemap');
    Route::get('/subdistrict-search', 'Alamat@subdistrict_search');
    Route::get('/kecamatan', 'Alamat@kecamatan');
});
Route::group(['prefix' => 'v1', 'namespace' => 'Inventory'], function () {

    Route::get('/produk', 'Produk@index');
    Route::post('/produk/insert', 'Produk@store');
    Route::post('/produk/update', 'Produk@update');
    Route::post('/produk/delete', 'Produk@delete');
    Route::post('/produk/detail', 'Produk@detail');

    Route::post('/produk-image/insert', 'ProdukImage@store');
    Route::post('/produk-image/update', 'ProdukImage@update');
    Route::post('/produk-image/delete', 'ProdukImage@delete');
    Route::post('/produk-image/detail', 'ProdukImage@detail');
    Route::post('/produk-image/sample', 'ProdukImage@sample');

    Route::get('/order', 'Order@index');
    Route::post('/order/insert', 'Order@store');
    Route::post('/order/update', 'Order@update');
    Route::post('/order/delete', 'Order@delete');
    Route::post('/order/detail', 'Order@detail');

    Route::post('/order/status', 'Order@status');

    Route::post('/order/potong-stock', 'Order@potong_stock');
    Route::post('/order/kembalikan-stock', 'Order@kembalikan');
    Route::post('/order/salin', 'Order@salin');
});


Route::group(['prefix' => 'mobile', 'namespace' => 'Mobile'], function () {
    Route::get('/produk', 'ProdukMob@data');

    Route::post('/dashboard', 'DashboardMob@show');
    // Route::get('/dashboards', 'DashboardMob@show');

    Route::post('/reseller', 'ResellerMob@show');
    Route::post('/reseller_nearby', 'ResellerMob@nearby');
    // Route::get('/resellers', 'ResellerMob@show');

    Route::post('/kabupaten', 'ResellerMob@lokasi_kabupaten');
    Route::post('/kecamatan', 'ResellerMob@lokasi_kecamatan');
});
