<?php

namespace App\Http\Controllers\MasterData;

use App\Helpers\General;
use App\Models\mCustomer;
use App\Rules\rCustomerStore;
use App\Rules\rCustomerUpdate;
use App\Rules\rLogin;
use App\Rules\rStaffStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class Customer extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jenis Customer
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $data = mCustomer::
        select('*')
            ->paginate(10);
        return General::response(General::$success, General::$get, $data);
    }

    /**
     *
     * Membuat Customer Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => ['required', new rCustomerStore],
            'alamat' => 'required',
            'kode_pos' => 'required',
            'telephone' => 'required',
            'password' => 'required',
            'confirm_password' => 'bail|required|same:password',
        ];

        $attributes = [
            'name' => 'Nama Customer',
            'email' => 'Email Customer',
            'alamat' => 'Alamat Customer',
            'kode_pos' => 'Kode Pos',
            'telephone' => 'Telephone',
            'password' => 'Password',
            'confirm_password' => 'Konfirmasi Password',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $name = $request->input('name');
            $email = $request->input('email');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $telephone = $request->input('telephone');
            $password = Hash::make($request->input('password'));

            $data = [
                'name' => $name,
                'email' => $email,
                'alamat' => $alamat,
                'kode_pos' => $kode_pos,
                'telephone' => $telephone,
                'password' => $password,
            ];

            mCustomer::create($data);
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    /**
     * Update Customer
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'customer_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'kode_pos' => 'required',
            'telephone' => 'required',
            'password' => 'required',
            'confirm_password' => 'bail|required|same:password',
        ];

        $attributes = [
            'customer_id' => 'ID Customer',
            'name' => 'Nama Customer',
            'email' => 'Email Customer',
            'alamat' => 'Alamat Customer',
            'kode_pos' => 'Kode Pos',
            'telephone' => 'Telephone',
            'password' => 'Password',
            'confirm_password' => 'Konfirmasi Password',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $customer_id = $request->input('customer_id');
        $rules = [
            'email' => new rCustomerUpdate($customer_id),
        ];
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $name = $request->input('name');
            $email = $request->input('email');
            $alamat = $request->input('alamat');
            $kode_pos = $request->input('kode_pos');
            $telephone = $request->input('telephone');
            $password = Hash::make($request->input('password'));

            $data = [
                'name' => $name,
                'email' => $email,
                'alamat' => $alamat,
                'kode_pos' => $kode_pos,
                'telephone' => $telephone,
                'password' => $password,
            ];
            mCustomer::where('customer_id', $customer_id)->update($data);

            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Customer
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'customer_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'customer_id' => 'ID Customer'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $customer_id = $request->input('customer_id');

        mCustomer::where(['customer_id' => $customer_id])->delete();
        return General::response(General::$success, General::$delete);
    }

    /**
     * Detail Jenis Customer
     */
    function detail(Request $request)
    {
        $rules = [
            'customer_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'customer_id' => 'ID Customer'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $customer_id = $request->input('customer_id');

        $data = mCustomer::select('*')
            ->find($customer_id);
        return General::response(General::$success, General::$get, $data);
    }
    function auth(Request $request)
    {
        $email = $request->input('email');
        $rules = [
            'email' => 'required',
            'password' => ['required', new rLogin($email)]
        ];

        General::validator($request->all(), $rules);


        $data_where = [
            'email' => $email
        ];


        $customer = mCustomer::
            where($data_where)
            ->first();

        $data = [
            'customer' => $customer,
        ];

        return General::response(General::$success, General::$login_success, $data);


    }
}


