<?php

namespace App\Http\Controllers\MasterData;

use App\Helpers\General;



use App\Models\mReseller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class Reseller extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jenis Customer
     *
     * @param Request $request
     * @return array
     */

    function active(Request $request){
        $province_id = $request->input("province_id");
        $city_id = $request->input("city_id");
        $subdistrict_id = $request->input("subdistrict_id");
        $username = $request->input("username");
        $reseller = mReseller::
        select(['reseller.*',
            'db2.username',
            'db2.avatar',
            'province.province',
            'city.city_name',
            'subdistrict.subdistrict_name',
        ])
            ->where('status','active')
            ->join('redpos_master_kutus.login as db2','reseller.reseller_id','=','db2.reseller_id')
            ->join('province','province.province_id','=','reseller.province_id')
            ->join('city','city.city_id','=','reseller.city_id')
            ->join('subdistrict','subdistrict.subdistrict_id','=','reseller.subdistrict_id')
        ;

        if($province_id!=""){
            $reseller->where('province.province_id',$province_id);
        }
        if($city_id!=""){
            $reseller->where('city.city_id',$city_id);
        }
        if($subdistrict_id!=""){
            $reseller->where('subdistrict.subdistrict_id',$subdistrict_id);
        }
        if($username!=""){
            $reseller->where('username',$username);
        }
        $data = $reseller->get();
        return General::response(General::$success, General::$get, $data);
    }
    function reseller_by_id(Request $request){
        $reseller_id = $request->input("reseller_id");
        $reseller = mReseller::
        select(['reseller.*',
            'db2.username',
            'db2.avatar',
            'province.province',
            'city.city_name',
            'subdistrict.subdistrict_name',
        ])
            ->where('status','active')
            ->where('reseller.reseller_id',$reseller_id)
            ->join('redpos_master_kutus.login as db2','reseller.reseller_id','=','db2.reseller_id')
            ->join('province','province.province_id','=','reseller.province_id')
            ->join('city','city.city_id','=','reseller.city_id')
            ->join('subdistrict','subdistrict.subdistrict_id','=','reseller.subdistrict_id')
        ;
        $data = $reseller->get();
        return General::response(General::$success, General::$get, $data);
    }

}


