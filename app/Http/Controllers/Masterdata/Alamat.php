<?php

namespace App\Http\Controllers\MasterData;

use App\Helpers\General;

use App\Models\mCity;
use App\Models\mProvince;
use App\Models\mSubdistrict;
use App\Rules\rStaffStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class Alamat extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jenis Customer
     *
     * @param Request $request
     * @return array
     */

    function province(Request $request){
        $search = $request->post("search");

        $data = mProvince::
        select('*')
            ->Where('province', 'like', '%' . $search . '%')
            ->get();
        return General::response(General::$success, General::$get, $data);
    }
    function city(Request $request){
        $search = $request->input("search");
        $rules = [
            'province_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'province_id' => 'ID Provinsi'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $province_id = $request->input('province_id');

        $data = mCity::select('*')
            ->where(['province_id'=>$province_id])
            ->where('city_name', 'like', '%' . $search . '%')
            ->get();
        return General::response(General::$success, General::$get, $data);
    }
    function subdistrict(Request $request){
        $search = $request->input("search");
        $rules = [
            'city_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'city_id' => 'ID Kabupaten'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $city_id = $request->input('city_id');

        $data = mSubdistrict::select('*')
            ->where(['city_id'=>$city_id])
            ->where('subdistrict_name', 'like', '%' . $search . '%')
            ->get();
        return General::response(General::$success, General::$get, $data);
    }
    function province_search(Request $request){
        $search = $request->input('search');
        $data = mProvince::
        select('*')

            ->Where('province',$search )
            ->first();
        return General::response(General::$success, General::$get, $data);
    }
    function city_search(Request $request){
        $search = $request->input('search');
        $data = mCity::select('*')
            ->where('city_name', $search )
            ->first();
        return General::response(General::$success, General::$get, $data);
    }
    function subdistrict_search(Request $request){

        $search = $request->input('search');
        $data = mSubdistrict::select('*')
            ->where('subdistrict_name',$search )
            ->first();
        return General::response(General::$success, General::$get, $data);
    }
    function kecamatan(Request $request){
        $search = $request->input('search');
        echo $search;
    }
    function sitemap(){
        $data = mProvince::
        select('*')
            ->with(['kota_nya'])
            ->with(['kota_nya.kecamatannya'])
            ->get();
        return General::response(General::$success, General::$get, $data);
    }
}


