<?php

namespace App\Http\Controllers\Masterdata;

use App\Helpers\General;
use App\Models\mJenisProduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class JenisProduk extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jenis Produk
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $data = mJenisProduk
            ::orderBy('jenis_produk_id', 'DESC')
            ->paginate(10);
        return General::response(General::$success, General::$get, $data);
    }

    /**
     *
     * Membuat Jenis Produk Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'jenis_produk_nama' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',
        ];


        $attributes = [
            'jenis_produk_nama' => 'Nama Jenis produk',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $jenis_produk_nama = $request->input('jenis_produk_nama');
            $meta_tile = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description = $request->input('meta_description');



            $data_anggaran = [
                'jenis_produk_nama' => $jenis_produk_nama,
                'meta_title' => $meta_tile,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
            ];

            mJenisProduk::create($data_anggaran);
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    /**
     * Update Jenis Produk
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'jenis_produk_id' => [
                'required',
                'integer',

            ],
            'jenis_produk_nama' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',
        ];

        $attributes = [
            'jenis_produk_id' => 'ID Jenis Produk',
            'jenis_produk_nama' => 'Nama Jenis Produk',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $jenis_produk_id = $request->input('jenis_produk_id');
            $jenis_produk_nama = $request->input('jenis_produk_nama');
            $meta_tile = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description = $request->input('meta_description');
            $data_anggaran = [
                'jenis_produk_nama' => $jenis_produk_nama,
                'meta_title' => $meta_tile,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
                'updated_at' => date("Y-m-d H:i:s"),
            ];

            mJenisProduk::where('jenis_produk_id', $jenis_produk_id)->update($data_anggaran);

            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Jenis Produk
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'jenis_produk_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'jenis_produk_id' => 'ID Jenis Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $jenis_produk_id = $request->input('jenis_produk_id');

        mJenisProduk::where(['jenis_produk_id' => $jenis_produk_id])->delete();
        return General::response(General::$success, General::$delete);

    }

    /**
     * Detail Jenis Produk
     */
    function detail(Request $request)
    {
        $rules = [
            'jenis_produk_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'jenis_produk_id' => 'ID Jenis Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $jenis_produk_id = $request->input('jenis_produk_id');

        $data = mJenisProduk::find($jenis_produk_id);
        return General::response(General::$success, General::$get, $data);
    }
    function jenis_dan_produk_nya(){
        $data = mJenisProduk::select(['*'])
        ->with([
            'produk_nya:produk_jenis_id,produk_nama',
            'produk_nya.gambar_nya:*',
            ])
            ->paginate(10);
        return $data;
    }
    function listCount(){
        $data = mJenisProduk::select(['*'])
            ->withCount(['produk_nya'])
            ->paginate(10);
        return $data;
    }
}


