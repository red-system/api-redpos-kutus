<?php

namespace App\Http\Controllers\Mobile;

use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Models\Mobile\mobArtikel;
use App\Models\Mobile\mobProduk;
use Illuminate\Support\Facades\DB;

class DashboardMob extends Controller
    {
        /**
         * Retrieve the user for the given ID.
         *
         * @param  int  $id
         * @return Response
         */
        public function show()
        {
            $artikel = mobArtikel::where('active','1')->get();
            $produk = mobProduk::with('satuan_produk','jenis_produk')->get();

            $output = [
                'artikel'    => $artikel,
                'produk'     => $produk,
            ];

            return General::response(General::$success, General::$get, $output);
            
        }
    }
?>
