<?php

namespace App\Http\Controllers\Mobile;

use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Models\Mobile\mobCity;
use App\Models\Mobile\mobDistrict;
use App\Models\Mobile\mobProvincie;
use App\Models\Mobile\mobReseller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResellerMob extends Controller
    {
        /**
         * Retrieve the user for the given ID.
         *
         * @param  int  $id
         * @return Response
         */

        public static function filter(){
            return [
                'reseller_id',
                'nama',
                'province_id',
                'city_id',
                'subdistrict_id',
                'email',
                'phone',
                'alamat',
                'foto_profile',
                'maps',
                'latitude',
                'longitude',
                'status',
                'type'
            ];
        }

        public function show()
        {
            $reseller   = mobReseller::where('status','active')->get($this->filter());
            $provincie  = mobProvincie::get();

            $output = [
                'reseller' => $reseller,
                'provincie' => $provincie,
            ];

            return General::response(General::$success, General::$get, $output);
        }

        public function nearby(Request $request)
        {
            $lat		= $request->latitude;
		    $lon		= $request->longitude;

            $query      = 'call sp_reseller_nearby('.$lat.','.$lon.', 5)';

            $resellerNearby = DB::select($query);
            $output['reseller_nearby'] = $resellerNearby;

            return General::response(General::$success, General::$get, $output);
        }

        public function lokasi_kabupaten(Request $request)
        {
            $provinceId = $request->id;
            $kabupaten  = mobCity::where('province_id',$provinceId)->get();

            $output['kabupaten'] = $kabupaten;

            return General::response(General::$success, General::$get, $output);
        }

        public function lokasi_kecamatan(Request $request)
        {
            $cityId     = $request->id;
            $kecamatan  = mobDistrict::where('city_id',$cityId)->get();


            $output['kecamatan'] = $kecamatan;

            return General::response(General::$success, General::$get, $output);
        }
    }
?>
