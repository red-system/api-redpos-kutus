<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ArusKas extends Controller
{

    public function __construct()
    {

    }

    /**
     *
     * Parameter tanggal awal dan tanggal akhir
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {
        $rules = [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
        ];

        $attributes = [
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $where_date = [
            $date_start,
            $date_end
        ];

        /**
         * Perhitungan Kas Kecil
         */
        $debet = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_debet');
        $kredit = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1101')
            ->sum('trs_kredit');
        $kas_kecil = $debet - $kredit;

        /**
         * Perhitungan Kas Besar
         */
        $debet_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_debet');
        $kredit_kas_besar = mAcTransaksi
            ::where('tgl_transaksi', '<', $date_start)
            ->where('trs_kode_rekening', '1102')
            ->sum('trs_kredit');
        $kas_besar = $debet_kas_besar - $kredit_kas_besar;
        $kas_awal = $kas_kecil + $kas_besar;

        /**
         * Arus Kas Kegiatan Operasi
         */
        $operasi_masuk = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_debet', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Operasi')
            ->where('trs_jenis_transaksi', 'debet')
            ->get();

        $operasi_keluar = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_kredit', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Operasi')
            ->where('trs_jenis_transaksi', 'kredit')
            ->get();

        $operasi_masuk_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Operasi')
            ->where('trs_jenis_transaksi', 'debet')
            ->sum('trs_debet');

        $operasi_keluar_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Operasi')
            ->where('trs_jenis_transaksi', 'kredit')
            ->sum('trs_kredit');

        $operasi_total = $operasi_masuk_total - $operasi_keluar_total;

        $operasi_data = [
            'label' => 'ARUS KAS DARI KEGIATAN OPERASI',
            'arus_kas_masuk' => [
                'label' => 'Arus Kas Masuk',
                'data' => $operasi_masuk
            ],
            'arus_kas_keluar' => [
                'label' => 'Arus Kas Keluar',
                'data' => $operasi_keluar
            ],
            'total' => [
                'label' => 'Total Kegiatan Operasi',
                'value' => $operasi_total
            ]
        ];

        /**
         * Arus Kas Kegiatan Pendanaan
         */

        $pendanaan_masuk = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_debet', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Pendanaan')
            ->where('trs_jenis_transaksi', 'debet')
            ->get();

        $pendanaan_keluar = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_kredit', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Pendanaan')
            ->where('trs_jenis_transaksi', 'kredit')
            ->get();

        $pendanaan_masuk_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Pendanaan')
            ->where('trs_jenis_transaksi', 'debet')
            ->sum('trs_debet');

        $pendanaan_keluar_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Pendanaan')
            ->where('trs_jenis_transaksi', 'kredit')
            ->sum('trs_kredit');

        $pendanaan_total = $pendanaan_masuk_total - $pendanaan_keluar_total;

        $pendanaan_data = [
            'label' => 'ARUS KAS DARI KEGIATAN PENDANAAN',
            'arus_kas_masuk' => [
                'label' => 'Arus Kas Masuk',
                'data' => $pendanaan_masuk
            ],
            'arus_kas_keluar' => [
                'label' => 'Arus Kas Keluar',
                'data' => $pendanaan_keluar
            ],
            'total' => [
                'label' => 'Total Kegiatan Pendanaan',
                'value' => $pendanaan_total
            ]
        ];


        /**
         * Arus Kas Kegiatan Investasi
         */

        $investasi_masuk = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_debet', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Investasi')
            ->where('trs_jenis_transaksi', 'debet')
            ->get();

        $investasi_keluar = mAcTransaksi
            ::with([
                'jurnal_umum:jurnal_umum_id,jmu_keterangan'
            ])
            ->select(['trs_kredit', 'jurnal_umum_id'])
            ->whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Investasi')
            ->where('trs_jenis_transaksi', 'kredit')
            ->get();

        $investasi_masuk_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Investasi')
            ->where('trs_jenis_transaksi', 'debet')
            ->sum('trs_debet');

        $investasi_keluar_total = mAcTransaksi
            ::whereBetween('tgl_transaksi', $where_date)
            ->whereIn('trs_kode_rekening', ['1101', '1102'])
            ->where('trs_tipe_arus_kas', 'Investasi')
            ->where('trs_jenis_transaksi', 'kredit')
            ->sum('trs_kredit');

        $investasi_total = $investasi_masuk_total - $investasi_keluar_total;

        $investasi_data = [
            'label' => 'ARUS KAS DARI KEGIATAN INVESTASI',
            'arus_kas_masuk' => [
                'label' => 'Arus Kas Masuk',
                'data' => $investasi_masuk
            ],
            'arus_kas_keluar' => [
                'label' => 'Arus Kas Keluar',
                'data' => $investasi_keluar
            ],
            'total' => [
                'label' => 'Total Kegiatan Investasi',
                'value' => $investasi_total
            ]
        ];

        /**
         * Summary
         */

        $total = $pendanaan_total + $operasi_total + $investasi_total;

        $data = [
            'operasi' => $operasi_data,
            'pendanaan' => $pendanaan_data,
            'investasi' => $investasi_data,
            'total' => $total,
            'saldo_awal' => $kas_awal,
            'saldo_akhir' => $kas_awal + $total
        ];

        return General::response(General::$success, General::$get, $data);
    }


}


