<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use App\Rules\rMasterIdBukuBesar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BukuBesar extends Controller
{

    public function __construct()
    {

    }

    /**
     *
     * Data input date_start dan date_end
     * data yang di return, data yang digunakan untuk tampilan neraca
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $rules = [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
            'master_id' => [
                'required',
                new rMasterIdBukuBesar()
            ]
        ];

        $attributes = [
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $where_date = [$date_start, $date_end];
        $master_id = $request->input('master_id');
        $select_field = ['master_id', 'mst_kode_rekening', 'mst_nama_rekening', 'mst_normal'];

        if ($master_id == 'all') {
            $master = mAcMaster::select($select_field);
        } else {
            $master_id_arr = json_decode($master_id, TRUE);
            $master = mAcMaster::select($select_field)->whereIn('master_id', $master_id_arr);
        }

        $master = $master
            ->with([
                'transaksi' => function ($query) use ($where_date) {
                    $query->select(['jurnal_umum_id','master_id','trs_debet','trs_kredit'])->whereBetween('tgl_transaksi', $where_date);
                },
                'transaksi.jurnal_umum' => function($query) {
                    $query->select(['jurnal_umum_id','jmu_tanggal','no_invoice','jmu_keterangan']);
                }
            ])
            ->orderBy('mst_kode_rekening', 'ASC')
            ->get();

        foreach($master as $row_1) {
            $total_debet = $row_1->transaksi->where('tgl_transaksi', '<', $date_start)->sum('trs_debet');
            $total_kredit = $row_1->transaksi->where('tgl_transaksi', '<', $date_start)->sum('trs_kredit');

            if($row_1->mst_normal == 'debet') {
                $master_saldo_awal_label = 'Saldo (Debet)';
                $master_saldo_awal_value = $total_debet - $total_kredit;
            } else {
                $master_saldo_awal_label = 'Saldo (Kredit)';
                $master_saldo_awal_value = $total_kredit - $total_debet;
            }

            $row_1->master_saldo_awal_label = $master_saldo_awal_label;
            $row_1->master_saldo_awal_value = $master_saldo_awal_value;

            $total_debet = 0;
            $total_kredit = 0;

            foreach($row_1->transaksi as $row_2) {
                $total_debet += $row_2->trs_debet;
                $total_kredit += $row_2->trs_kredit;

                if($row_1->mst_normal == 'debet') {
                    $master_saldo_awal_value = $master_saldo_awal_value - $row_2->trs_kredit + $row_2->trs_debet;
                } else {
                    $master_saldo_awal_value = $master_saldo_awal_value + $row_2->trs_kredit - $row_2->trs_debet;
                }

                $row_2->saldo_now = $master_saldo_awal_value;
            }

            $row_1->master_saldo_akhir_value = $master_saldo_awal_value;
            $row_1->total_debet = $total_debet;
            $row_1->total_kredit = $total_kredit;
        }


        $data = [
            'date_start' => $date_start,
            'date_end' => $date_end,
            'buku_besar' => $master,
        ];

        return General::response(General::$success, General::$get, $data);

    }

}


