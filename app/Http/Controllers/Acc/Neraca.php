<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Neraca extends Controller
{

    public function __construct()
    {

    }

    /**
     *
     * Data input date_start dan date_end
     * data yang di return, data yang digunakan untuk tampilan neraca
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $rules = [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
        ];

        $attributes = [
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        $total_asset = 0;
        $total_liabilitas = 0;
        $total_ekuitas = 0;

        $asset_temp = [];
        $ekuitas_temp = [];
        $liabilitas_temp = [];

        $data_asset = mAcMaster
            ::where('mst_neraca_tipe', 'asset')
            ->where('mst_master_id', 0)
            ->with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },
                'childs.childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },

                'transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                }
            ])
            ->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_normal'])
            ->get();

        $data_ekuitas_list = mAcMaster
            ::where('mst_neraca_tipe', 'ekuitas')
            ->where('mst_master_id', 0)
            ->with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening']);
                },
                'childs.childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening']);
                },

                'transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                }

            ])
            ->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_normal', 'mst_kode_rekening'])
            ->get();

        $data_liabilitas_list = mAcMaster
            ::where('mst_neraca_tipe', 'liabilitas')
            ->with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },
                'childs.childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal']);
                },

                'transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                },
                'childs.childs.childs.transaksi' => function ($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit', 'trs_debet'])
                        ->where('tgl_transaksi', '<=', $date_end);
                }
            ])
            ->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_normal'])
            ->get();


        $detail_perkiraan = mAcMaster
            ::with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
            ])
            ->select(['master_id', 'mst_neraca_tipe', 'mst_master_id', 'mst_kode_rekening', 'mst_nama_rekening'])
            ->get();

        /**
         * Data Asset
         */
        foreach ($data_asset as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    if ($asset_childs_2->mst_normal == 'kredit') {
                        $kredit = $asset_childs_2->transaksi->sum('trs_kredit');
                        $debet = $asset_childs_2->transaksi->sum('trs_debet');
                        $asset = $kredit - $debet;
                        $asset_temp[$asset_childs_2->master_id] = $asset;
                        $total_asset -= $asset;
                        $total_asset_childs -= $asset;

                    }
                    if ($asset_childs_2->mst_normal == 'debet') {
                        $kredit = $asset_childs_2->transaksi->sum('trs_kredit');
                        $debet = $asset_childs_2->transaksi->sum('trs_debet');
                        $asset = $debet - $kredit;
                        $asset_temp[$asset_childs_2->master_id] = $asset;
                        $total_asset += $asset;
                        $total_asset_childs += $asset;
                    }

                }

                if ($asset_childs->mst_normal == 'kredit') {
                    $kredit = $asset_childs->transaksi->sum('trs_kredit');
                    $debet = $asset_childs->transaksi->sum('trs_debet');
                    $asset = $kredit - $debet;
                    $asset_temp[$asset_childs->master_id] = $asset - $total_asset_childs;
                    $total_asset -= $asset;
                    $total_detail_asset -= $asset_temp[$asset_childs->master_id];
                }
                if ($asset_childs->mst_normal == 'debet') {
                    $kredit = $asset_childs->transaksi->sum('trs_kredit');
                    $debet = $asset_childs->transaksi->sum('trs_debet');
                    $asset = $debet - $kredit;
                    $asset_temp[$asset_childs->master_id] = $asset + $total_asset_childs;
                    $total_asset += $asset;
                    $total_detail_asset += $asset_temp[$asset_childs->master_id];
                }
            }

            if ($detail_asset->mst_normal == 'kredit') {
                $kredit = $detail_asset->transaksi->sum('trs_kredit');
                $debet = $detail_asset->transaksi->sum('trs_debet');
                $asset = $kredit - $debet;
                $asset_temp[$detail_asset->master_id] = $asset - $total_detail_asset;
                $total_asset -= $asset;
            }

            if ($detail_asset->mst_normal == 'debet') {
                $kredit = $detail_asset->transaksi->sum('trs_kredit');
                $debet = $detail_asset->transaksi->sum('trs_debet');
                $asset = $debet - $kredit;
                $asset_temp[$detail_asset->master_id] = $asset + $total_detail_asset;
                $total_asset += $asset;
            }
        }

        /**
         * Data Ekuitas
         */
        foreach ($data_ekuitas_list as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                if ($ekuitas_childs->mst_normal == 'kredit') {
                    $kredit = $ekuitas_childs->transaksi->sum('trs_kredit');
                    $debet = $ekuitas_childs->transaksi->sum('trs_debet');
                    $ekuitas = $kredit - $debet;
                    $ekuitas_temp[$ekuitas_childs->master_id] = $ekuitas;
                    $total_ekuitas += $ekuitas;
                    $total_data_ekuitas += $ekuitas;
                }
                if ($ekuitas_childs->mst_normal == 'debet') {
                    $kredit = $ekuitas_childs->transaksi->sum('trs_kredit');
                    $debet = $ekuitas_childs->transaksi->sum('trs_debet');
                    $ekuitas = $debet - $kredit;
                    $ekuitas_temp[$ekuitas_childs->master_id] = $ekuitas;
                    $total_ekuitas -= $ekuitas;
                    $total_data_ekuitas -= $ekuitas;
                }
                if ($ekuitas_childs->mst_kode_rekening == '2403') {
                    $ekuitas = $this->count_rugi_laba($date_end);
                    $ekuitas_temp[$ekuitas_childs->master_id] = $ekuitas;
                    $total_ekuitas += $ekuitas;
                    $total_data_ekuitas += $ekuitas;
                }
            }

            if ($data_ekuitas->mst_normal == 'kredit') {
                $kredit = $data_ekuitas->transaksi->sum('trs_kredit');
                $debet = $data_ekuitas->transaksi->sum('trs_debet');
                $ekuitas = $kredit - $debet;
                $ekuitas_temp[$data_ekuitas->master_id] = $ekuitas + $total_data_ekuitas;
            }
            if ($data_ekuitas->mst_normal == 'debet') {
                $kredit = $data_ekuitas->transaksi->sum('trs_kredit');
                $debet = $data_ekuitas->transaksi->sum('trs_debet');
                $ekuitas = $debet - $kredit;
                $ekuitas_temp[$data_ekuitas->master_id] = $ekuitas - $total_data_ekuitas;
            }

        }

//


        /**
         * Data Liabilitas
         */
        foreach ($data_liabilitas_list as $data_liabilitas) {
            $total_data_liabilitas = 0;

            foreach ($data_liabilitas->childs as $liabilitas_childs) {

                if ($liabilitas_childs->mst_normal == 'kredit') {
                    $kredit = $liabilitas_childs->transaksi->sum('trs_kredit');
                    $debet = $liabilitas_childs->transaksi->sum('trs_debet');
                    $liabilitas = $kredit - $debet;
                    $liabilitas_temp[$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas += $liabilitas;
                    } else {
                        $total_data_liabilitas -= $liabilitas;
                    }
                }

                if ($liabilitas_childs->mst_normal == 'debet') {
                    $kredit = $liabilitas_childs->transaksi->sum('trs_kredit');
                    $debet = $liabilitas_childs->transaksi->sum('trs_debet');
                    $liabilitas = $debet - $kredit;
                    $liabilitas_temp[$liabilitas_childs->master_id] = $liabilitas;
                    if ($data_liabilitas->mst_normal == 'kredit') {
                        $total_data_liabilitas -= $liabilitas;
                    } else {
                        $total_data_liabilitas += $liabilitas;
                    }
                }

            }

            if ($data_liabilitas->mst_normal == 'kredit') {
                $kredit = $data_liabilitas->transaksi->sum('trs_kredit');
                $debet = $data_liabilitas->transaksi->sum('trs_debet');
                $liabilitas = $kredit - $debet;
                $liabilitas_temp[$data_liabilitas->master_id] = $liabilitas + $total_data_liabilitas;
            }

            if ($data_liabilitas->mst_normal == 'debet') {
                $kredit = $data_liabilitas->transaksi->sum('trs_kredit');
                $debet = $data_liabilitas->transaksi->sum('trs_debet');
                $liabilitas = $debet - $kredit;
                $liabilitas_temp[$data_liabilitas->master_id] = $liabilitas - $total_data_liabilitas;
            }


            $total_liabilitas += $total_data_liabilitas;
        }


        $hitung = $total_liabilitas + $total_ekuitas;

        if (round($hitung, 2) == round($total_asset, 2)) {
            $status = 'BALANCE';
            $selisih = 0;
        } else {
            $status = 'NOT BALANCE';
            $selisih = $total_asset - $hitung;
        }

        $activa_data = [];
        $no = 0;

        foreach ($detail_perkiraan as $key => $row) {
            if ($row->mst_neraca_tipe == 'asset' && $row->mst_master_id == '0' && $row->mst_nama_rekening != 'AKTIVA TETAP') {

                $activa_data[$no] = [
                    'master_id' => $row->master_id,
                    'kode_rekening' => $row->mst_kode_rekening,
                    'nama_rekening' => $row->mst_nama_rekening,
                    'value' => $asset_temp[$row->master_id],
                ];

                $data_child = [];
                foreach ($row->childs as $key_1 => $row_1) {
                    $data_child[$key_1] = [
                        'master_id' => $row_1->master_id,
                        'mst_kode_rekening' => $row_1->mst_kode_rekening,
                        'mst_nama_rekening' => $row_1->mst_nama_rekening,
                        'value' => $asset_temp[$row_1->master_id]
                    ];
                }

                $activa_data[$no]['childs'] = $data_child;
                $no++;
            }

            if ($row->mst_neraca_tipe == 'asset' && $row->mst_master_id == '0' && $row->mst_nama_rekening == 'AKTIVA TETAP') {

                $activa_data[$no] = [
                    'master_id' => $row->master_id,
                    'kode_rekening' => $row->mst_kode_rekening,
                    'nama_rekening' => $row->mst_nama_rekening,
                    'value' => $asset_temp[$row->master_id],
                ];

                $data_child = [];
                foreach ($row->childs as $key_1 => $row_1) {
                    $data_child[$key_1] = [
                        'master_id' => $row_1->master_id,
                        'mst_kode_rekening' => $row_1->mst_kode_rekening,
                        'mst_nama_rekening' => $row_1->mst_nama_rekening,
                        'value' => $asset_temp[$row_1->master_id]
                    ];

                    foreach ($row_1->childs as $key_2 => $row_2) {
                        $data_child[$key_1]['childs'][$key_2] = [
                            'master_id' => $row_1->master_id,
                            'mst_kode_rekening' => $row_2->mst_kode_rekening,
                            'mst_nama_rekening' => $row_2->mst_nama_rekening,
                            'value' => $asset_temp[$row_2->master_id]
                        ];
                    }
                }

                $activa_data[$no]['childs'] = $data_child;

                $no++;
            }
        }

        $passiva_data = [];
        $no = 0;
        foreach ($detail_perkiraan as $key => $row) {
            if ($row->mst_neraca_tipe == 'liabilitas' && $row->mst_master_id == '0') {
                $passiva_data[$no] = [
                    'master_id' => $row->master_id,
                    'mst_kode_rekening' => $row->mst_kode_rekening,
                    'mst_nama_rekening' => $row->mst_nama_rekening,
                    'value' => $liabilitas_temp[$row->master_id]
                ];

                $data_child = [];
                foreach ($row->childs as $key_1 => $row_1) {
                    $data_child[$key_1] = [
                        'master_id' => $row->master_id,
                        'mst_kode_rekening' => $row_1->mst_kode_rekening,
                        'mst_nama_rekening' => $row_1->mst_nama_rekening,
                        'value' => $liabilitas_temp[$row_1->master_id]
                    ];
                }


                $passiva_data[$no]['childs'] = $data_child;
                $no++;
            }

            if ($row->mst_neraca_tipe == 'ekuitas' && $row->mst_master_id == '0') {
                $passiva_data[$no] = [
                    'master_id' => $row->master_id,
                    'mst_kode_rekening' => $row->mst_kode_rekening,
                    'mst_nama_rekening' => $row->mst_nama_rekening,
                    'value' => $ekuitas_temp[$row->master_id]
                ];

                $data_child = [];
                foreach ($row->childs as $key_1 => $row_1) {
                    $data_child[$key_1] = [
                        'master_id' => $row->master_id,
                        'mst_kode_rekening' => $row_1->mst_kode_rekening,
                        'mst_nama_rekening' => $row_1->mst_nama_rekening,
                        'value' => $ekuitas_temp[$row_1->master_id]
                    ];
                }


                $passiva_data[$no]['childs'] = $data_child;
                $no++;
            }
        }

        $total_asset = round($total_asset, 2);
        $total_ekuitas = round($total_ekuitas, 2);
        $total_liabilitas = round($total_liabilitas, 2);

        $data = [
            'total_asset' => $total_asset,
            'total_ekuitas' => $total_ekuitas,
            'total_liabilitas' => $total_liabilitas,

            'hitung' => $hitung,
            'status' => $status,
            'selisih' => $selisih,

            'activa_data' => $activa_data,
            'activa_total' => $total_asset,

            'passiva_data' => $passiva_data,
            'passiva_total' => $hitung,
        ];

        return General::response(General::$success, General::$get, $data);
    }

    function count_rugi_laba($tanggal)
    {
        $date_end = $tanggal;
        $laba_rugi = 0;

        $kode_pendapatan = mAcMaster
            ::select(['master_id', 'mst_normal'])
            ->where('mst_posisi', 'laba rugi')
            ->with([
                'transaksi' => function($query) use ($date_end) {
                    $query->select(['master_id','trs_kredit','trs_debet'])->where('tgl_transaksi', '<=', $date_end);
                }
            ])
            ->get();

        foreach ($kode_pendapatan as $perkiraan) {

            $kredit = $perkiraan->transaksi->sum('trs_kredit');
            $debet = $perkiraan->transaksi->sum('trs_debet');

            if ($perkiraan->mst_normal == 'kredit') {
                $total_kalkulasi = $kredit - $debet;
                $laba_rugi = $laba_rugi + $total_kalkulasi;

            }
            if ($perkiraan->mst_normal == 'debet') {
                $total_kalkulasi = $debet - $kredit;
                $laba_rugi = $laba_rugi - $total_kalkulasi;
            }
        }
        return $laba_rugi;
    }

}


