<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Models\mAcAsset;
use App\Models\mAcAssetKategori;
use App\Rules\rIdAssetKategori;
use Illuminate\Http\Request;

class AssetKategori extends Controller
{

    public function __construct()
    {

    }

    /**
     * Mendapatkan list kategori asset
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {
        $data = mAcAssetKategori
            ::orderBy('nama', 'ASC')
            ->get();
        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Menambah kategori asset
     *
     * @param Request $request
     * @return array
     */
    function store(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'penyusutan_status' => 'required|in:yes,no',
            'description' => 'required',
        ];

        $attributes = [
            'nama' => 'Nama Kategori Asset',
            'penyusutan_status' => 'Status Penyusutan Kategori Asset',
            'description' => 'Deskripsi Kategori Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $data = [
            'nama' => $request->input('nama'),
            'penyusutan_status' => $request->input('penyusutan_status'),
            'description' => $request->input('description')
        ];

        mAcAssetKategori::create($data);

        return General::response(General::$success, General::$store);
    }

    /**
     * Mendapatkan detail row data kategori asset
     * @param Request $request
     * @return array
     */
    function detail(Request $request)
    {
        $rules = [
            'id_asset_kategori' => [
                'required',
                new rIdAssetKategori()
            ],
        ];

        $attributes = [
            'id_asset_kategori' => 'ID Kategori Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $id_asset_kategori = $request->input('id_asset_kategori');
        $data = mAcAssetKategori::where('id', $id_asset_kategori)->first();

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Update kategori asset
     *
     * @param Request $request
     * @return array
     */
    function update(Request $request)
    {
        $rules = [
            'id_asset_kategori' => [
                'required',
                new rIdAssetKategori()
            ],
            'nama' => 'required',
            'penyusutan_status' => 'required|in:yes,no',
            'description' => 'required',
        ];

        $attributes = [
            'id_asset_kategori' => 'ID Kategori Asset',
            'nama' => 'Nama Kategori Asset',
            'penyusutan_status' => 'Status Penyusutan Kategori Asset',
            'description' => 'Deskripsi Kategori Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $id_asset_kategori = $request->input('id_asset_kategori');

        $data = [
            'nama' => $request->input('nama'),
            'penyusutan_status' => $request->input('penyusutan_status'),
            'description' => $request->input('description')
        ];

        mAcAssetKategori::where('id', $id_asset_kategori)->update($data);

        return General::response(General::$success, General::$update);
    }

    /**
     * Delete Kategori Asset
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'id_asset_kategori' => [
                'required',
                new rIdAssetKategori()
            ],
        ];

        $attributes = [
            'id_asset_kategori' => 'ID Kategori Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $id_asset_kategori = $request->input('id_asset_kategori');
        mAcAssetKategori::where('id', $id_asset_kategori)->delete();

        return General::response(General::$success, General::$delete);
    }


}


