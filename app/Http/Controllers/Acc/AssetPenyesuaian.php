<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\Main;
use App\Helpers\hAkunting;
use App\Models\mAcAsset;
use App\Models\mAcAssetKategori;
use App\Models\mAcAssetPenyusutan;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class AssetPenyesuaian extends Controller
{
    private $datetime;

    function __construct()
    {
        $this->datetime = date('Y-m-d H:i:s');
    }

    /**
     * Dalam cron ini, asset penyusutan di proses pada akhir tanggal di akhir bulan,
     * untuk melakukan penyusutan pada asset,
     * dengan alur :
     * 1. jika kategori asset, melakukan penyusutan_status = 'yes', maka dilakukan penyusutan
     * 2. memperbarui nilai buku dan akumulasi beban pada asset
     * 3. membuat jurnal umum dan transaksi
     *
     */
    function cron_penyusutan_asset()
    {

        /**
         * Check jika hari ini adalah akhir bulan
         */
        if (gmdate('t') == gmdate('d')) {

            $asset = mAcAsset::all();
            foreach ($asset as $r_asset) {
                \DB::beginTransaction();
                try {

                    $check_asset_penyusutan = mAcAssetPenyusutan
                        ::where([
                            'id_asset' => $r_asset->id,
                            'bulan' => date('m'),
                            'tahun' => date('Y')
                        ])
                        ->count();

                    $penyusutan_status = mAcAssetKategori::where('id', $r_asset->id_asset_kategori)->value();

                    /**
                     * Check jika penyusutan akun belum dilakukan pada bulan ini dan penyusutan status terhadap asset adalah yes
                     */
                    if ($check_asset_penyusutan == 0 && $penyusutan_status == 'yes') {

                        $id_asset = $r_asset->id;
                        $beban_perbulan = $r_asset->beban_perbulan;
                        $akumulasi_beban = $r_asset->akumulasi_beban;
                        $nilai_buku = $r_asset->nilai_buku;
                        $nama = $r_asset->nama;
                        $master_id_asset = $r_asset->master_id_asset;
                        $master_id_depresiasi = $r_asset->master_id_depresiasi;
                        $master_id_akumulasi = $r_asset->master_id_akumulasi;
                        $tanggal_transaksi = $this->datetime;
                        $no_transaksi = hAkunting::kode_asset();
                        $keterangan_depresiasi = "Penyusutan Asset : " . $nama;
                        $keterangan_akumulasi = "Penyusutan Asset : " . $nama;
                        $year = date('Y', strtotime($tanggal_transaksi));
                        $month = date('m', strtotime($tanggal_transaksi));
                        $day = date('d', strtotime($tanggal_transaksi));
                        $jmu_no = hAkunting::jmu_no($year, $month, $day);

                        $master_asset = mAcMaster::where('master_id', $master_id_asset)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                        $master_depresiasi = mAcMaster::where('master_id', $master_id_depresiasi)->first(['mst_kode_rekening', 'mst_nama_rekening']);
                        $master_akumulasi = mAcMaster::where('master_id', $master_id_akumulasi)->first(['mst_kode_rekening', 'mst_nama_rekening']);

                        $data_asset = [
                            'akumulasi_beban' => $akumulasi_beban,
                            'nilai_buku' => $nilai_buku
                        ];

                        mAcAsset::where('id', $id_asset)->update($data_asset);


                        //insert ke tb_penyusutan_asset
                        $data_penyusutan = [
                            'id_asset' => $id_asset,
                            'penyusutan_perbulan' => $beban_perbulan,
                            'bulan' => $month,
                            'tahun' => $year
                        ];
                        $response = mAcAssetPenyusutan::create($data_penyusutan);
                        $id_penyusutan_asset = $response->id;


                        $data_jurnal = [
                            'no_invoice' => $no_transaksi,
                            'id_asset' => $id_asset,
                            'id_penyusutan_asset' => $id_penyusutan_asset,
                            'jmu_tanggal' => Main::format_date_db($tanggal_transaksi),
                            'jmu_no' => $jmu_no,
                            'jmu_year' => $year,
                            'jmu_month' => $month,
                            'jmu_day' => $day,
                            'jmu_keterangan' => 'Akumulasi Penyusutan ' . $master_asset->mst_nama_rekening . ' Asset No : ' . $no_transaksi,
                        ];
                        $response = mAcJurnalUmum::create($data_jurnal);
                        $jurnal_umum_id = $response->jurnal_umum_id;

                        $data_transaksi = [
                            'jurnal_umum_id' => $jurnal_umum_id,
                            'id_asset' => $id_asset,
                            'master_id' => $master_id_depresiasi,
                            'trs_jenis_transaksi' => 'debet',
                            'trs_debet' => $beban_perbulan,
                            'trs_kredit' => 0,
                            'user_id' => 0,
                            'trs_year' => $year,
                            'trs_month' => $month,
                            'trs_kode_rekening' => $master_depresiasi->mst_kode_rekening,
                            'trs_nama_rekening' => $master_depresiasi->mst_nama_rekening,
                            'trs_tipe_arus_kas' => 'Operasi',
                            'trs_catatan' => $keterangan_depresiasi . ' : ' . $master_depresiasi->mst_nama_rekening . ' ' . $nama,
                            'trs_charge' => 0,
                            'trs_no_check_bg' => '',
                            'trs_tgl_pencairan' => $this->datetime,
                            'trs_setor' => 0,
                            'tgl_transaksi' => Main::format_date_db($tanggal_transaksi),
                        ];

                        mAcTransaksi::create($data_transaksi);

                        $data_transaksi_akumulasi = [
                            'jurnal_umum_id' => $jurnal_umum_id,
                            'id_asset' => $id_asset,
                            'master_id' => $master_id_akumulasi,
                            'trs_jenis_transaksi' => 'kredit',
                            'trs_debet' => 0,
                            'trs_kredit' => $beban_perbulan,
                            'user_id' => 0,
                            'trs_year' => $year,
                            'trs_month' => $month,
                            'trs_kode_rekening' => $master_akumulasi->mst_kode_rekening,
                            'trs_nama_rekening' => $master_akumulasi->mst_nama_rekening,
                            'trs_tipe_arus_kas' => 'operasi',
                            'trs_catatan' => $keterangan_akumulasi . ' : ' . $master_akumulasi->mst_nama_rekening . ' ' . $nama,
                            'trs_charge' => 0,
                            'trs_no_check_bg' => '',
                            'trs_tgl_pencairan' => $this->datetime,
                            'trs_setor' => 0,
                            'tgl_transaksi' => Main::format_date_db($tanggal_transaksi),
                        ];

                        mAcTransaksi::create($data_transaksi_akumulasi);

                        \DB::commit();
                    }


                } catch (Exception $e) {
                    throw $e;
                    \DB::rollBack();
                }
            }
        }
    }

}