<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class JurnalUmum extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jurnal Umum yanng digunakann dalam view akunting
     *
     * @param Request $request
     * @return array
     */
    function list(Request $request)
    {

        $rules = [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
        ];

        $attributes = [
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $jurnal_umum = [];
        $where_date = [
            $date_start,
            $date_end
        ];

        $data_jurnal_umum = mAcJurnalUmum
            ::with([
                'transaksi' => function ($query) {
                    $query->select([
                        'jurnal_umum_id', 'trs_nama_rekening', 'trs_kode_rekening', 'trs_debet', 'trs_kredit', 'trs_catatan'
                    ]);
                }
            ])
            ->whereBetween('jmu_tanggal', $where_date)
            ->select(['jurnal_umum_id', 'jmu_tanggal', 'no_invoice', 'jmu_keterangan'])
            ->orderBy('jurnal_umum_id', 'ASC')
            ->get();

        foreach ($data_jurnal_umum as $key => $row) {
            $balance_status = TRUE;
            $balance_selisih = 0;
            $total_debet = 0;
            $total_kredit = 0;
            foreach ($row->transaksi as $key_1 => $row_1) {
                $total_debet += $row_1->trs_debet;
                $total_kredit += $row_1->trs_kredit;
            }

            if ($total_debet != $total_kredit) {
                $balance_status = FALSE;
                $balance_selisih = $total_debet > $total_kredit ? $total_debet - $total_kredit : $total_kredit - $total_debet;
            }

            $jurnal_umum[$key] = $row;
            $jurnal_umum[$key]['kredit_total'] = $total_kredit;
            $jurnal_umum[$key]['debet_total'] = $total_debet;
            $jurnal_umum[$key]['balance_status'] = $balance_status;
            $jurnal_umum[$key]['balance_selisih'] = $balance_selisih;
        }


        $total_debet = mAcTransaksi
            ::whereDate('tgl_transaksi', $where_date)
            ->sum('trs_debet');
        $total_kredit = mAcTransaksi
            ::whereDate('tgl_transaksi', $where_date)
            ->sum('trs_kredit');

        if ($total_debet == $total_kredit) {
            $balance_status = TRUE;
            $balance_selisih = 0;
        } else {
            $balance_status = FALSE;
            $balance_selisih = $total_debet > $total_kredit ? $total_debet - $total_kredit : $total_kredit - $total_debet;
        }

        $data = [
            'jurnal_umum' => $jurnal_umum,
            'date_start' => $date_start,
            'date_end' => $date_end,
            'total_debet' => $total_debet,
            'total_kredit' => $total_kredit,
            'balance_status' => $balance_status,
            'balance_selisih' => $balance_selisih
        ];

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Data-data yang diperlukan saat membuat create jurnal umum
     *
     * @return array
     */
    function create()
    {
        $kodeJurnalUmum = 'JU';
        $no_invoice = hAkunting::getNoTransaksiPayment($kodeJurnalUmum);
//        $master = mAcMaster::orderBy('master_id', 'ASC')->get();
        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();

        $data = [
            'no_invoice' => $no_invoice,
//            'master' => $master,
            'kode_perkiraan_option_list' => $kode_perkiraan_option_list
        ];

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Saat membuat jurnal umum ada data yang diperlukan, seperti:
     * 1. Data Input Jurnal Umum
     * 2. Data Input Transaksi untuk Jurnal Umum
     *
     * Notes :
     * Field jmu_tipe_input ini berisikan tipe data enum dengan value
     * jurnal_umum & menu_lain
     * 1. jika value jurnal_umum : tidak perlu mengisikan value pada :
     *         jmu_field_value, jmu_field_name, jmu_table_name, trs_field_value, trs_field_name dan trs_table_name
     * 2. jika value menu_lain : harus mengisikan value pada :
     *         jmu_field_value, jmu_field_name, jmu_table_name, trs_field_value, trs_field_name dan trs_table_name
     *    karena data yang masuk ke jurnal umum dan transaksi
     *    berasal dari menu-menu yang berkaitan dengan akunting, seperti menu produksi, penjualan, pembelian dan lainnya
     *    yang akan langsung masuk ke jurnal umum, sehingga diperlukan table_id dan table_name dari asal mana data
     *    untuk jurnal umum itu dimasukkan.
     *    Misaalnya: pada menu pembelian, ada transaksi akunting
     *       sehingga data pada jurnal umum
     *          jmu_field_value = 1 (increment dari primary_key bersangkutan)
     *          jmu_field_name = id_pembelian (nama field primary_key)
     *          jmu_table_name = tb_pembelian (nama table)
     *
     *          trs_field_value = 24 (increment dari primary_key bersangkutan)
     *          trs_field_name = id_pembelian_detail (nama field primary_key)
     *          trs_table_name = tb_pembelian_detail (nama table)
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $jmu_tipe_input = $request->input('jmu_tipe_input');
        $rules = [
            'user_id' => 'required',
            'user_name' => 'required',
            'user_table_name' => 'required',
            'jmu_tanggal' => 'required|date_format:Y-m-d',
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'jmu_tipe_input' => 'required|in:jurnal_umum,menu_lain',
            'master_id' => [
                'required',
                'array',
                new rMasterId()
            ],
            'master_id.*' => 'required',
            'trs_jenis_tranksasi.*' => 'required|in:debet,kredit',
            'trs_tipe_arus_kas.*' => 'required',
            'trs_catatan.*' => 'required',
            'trs_debet' => 'required|array',
            'trs_debet.*' => 'required',
            'trs_kredit.*' => 'required',
            'trs_kredit' => [
                'required',
                'array',
                new rJurnalUmumBalance($request->input('trs_debet'))
            ],
        ];

        if ($jmu_tipe_input == 'menu_lain') {
            $rules['jmu_field_value'] = 'required';
            $rules['jmu_field_name'] = 'required';
            $rules['jmu_table_name'] = 'required';

            $rules['trs_field_value'] = 'required|array';
            $rules['trs_field_value.*'] = 'required';
            $rules['trs_field_name'] = 'required|array';
            $rules['trs_field_name.*'] = 'required';
            $rules['trs_table_name'] = 'required|array';
            $rules['trs_table_name.*'] = 'required';
        }


        $attributes = [
            'user_id' => 'ID User',
            'user_name' => 'Nama User',
            'user_table_name' => 'Nama Table User',
            'jmu_tanggal' => 'Tanggal Jurnal Umum',
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'jurnal_umum_tipe' => 'Tipe Jurnal Umum',
            'master_id' => 'ID COA',
            'trs_jenis_transaksi' => 'Jenis Transaksi',
            'trs_tipe_arus_kas' => 'Tipe Arus Kas',
            'trs_catatan' => 'Catatan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit'
        ];

        if ($jmu_tipe_input == 'menu_lain') {
            $attributes['jmu_field_value'] = 'Value Field Jurnal Umum';
            $attributes['jmu_field_name'] = 'Nama Field Jurnal Umum';
            $attributes['jmu_table_name'] = 'Nama Tabel Junrnal Umum';

            $attributes['trs_field_value'] = 'Value Field Transaksi';
            $attributes['trs_field_name'] = 'Nama Field  Transaksi';
            $attributes['trs_table_name'] = 'Nama Tabel  Transaksi';
        }


        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['trs_jenis_transaksi.' . $i] = 'Jenis Transaksi ke-' . $next;
            $attr['trs_tipe_arus_kas.' . $i] = 'Tipe Arus Kas ke-' . $next;
            $attr['trs_catatan.' . $i] = 'Catatan ke-' . $next;
            $attr['trs_debet.' . $i] = 'Nominal Debet ke-' . $next;
            $attr['trs_kredit.' . $i] = 'Nominal Kredit ke-' . $next;

            if ($jmu_tipe_input == 'menu_lain') {
                $attr['trs_field_value.' . $i] = 'Value Field Transaksi Database ke-' . $next;
                $attr['trs_field_name.' . $i] = 'Nama Field Transaksi Database ke-' . $next;
                $attr['trs_table_name.' . $i] = 'Nama Tabel Transaksi Database ke-' . $next;
            }
        }


        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $user_id = $request->input('user_id');
            $user_name = $request->input('user_name');
            $user_table_name = $request->input('user_table_name');
            $jmu_tanggal = $request->input('jmu_tanggal');
            $no_invoice = $request->input('no_invoice');
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $jmu_keterangan = $request->input('jmu_keterangan');
            $jmu_field_value = $request->input('jmu_field_value');
            $jmu_field_name = $request->input('jmu_field_name');
            $jmu_table_name = $request->input('jmu_table_name');

            $master_id_arr = $request->input('master_id');
            $trs_field_value_arr = $request->input('trs_field_value');
            $trs_field_name_arr = $request->input('trs_field_name');
            $trs_table_name_arr = $request->input('trs_table_name');
            $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
            $trs_debet_arr = $request->input('trs_debet');
            $trs_kredit_arr = $request->input('trs_kredit');
            $trs_tipe_arus_kas_arr = $request->input('trs_tipe_arus_kas');
            $trs_catatan_arr = $request->input('trs_catatan');


            $data_jurnal_umum = [
                'user_id' => $user_id,
                'user_name' => $user_name,
                'user_table_name' => $user_table_name,
                'no_invoice' => $no_invoice,
                'jmu_tanggal' => $jmu_tanggal,
                'jmu_tipe_input' => $jmu_tipe_input,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan
            ];

            if ($jmu_tipe_input == 'menu_lain') {
                $data_jurnal_umum['jmu_field_value'] = $jmu_field_value;
                $data_jurnal_umum['jmu_field_name'] = $jmu_field_name;
                $data_jurnal_umum['jmu_table_name'] = $jmu_table_name;
            }

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first([
                        'mst_kode_rekening',
                        'mst_nama_rekening'
                    ]);
                $trs_field_value = $trs_field_value_arr[$index];
                $trs_field_name = $trs_field_name_arr[$index];
                $trs_table_name = $trs_table_name_arr[$index];
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_jenis_transaksi = $trs_jenis_transaksi_arr[$index];
                $trs_debet = $trs_debet_arr[$index];
                $trs_kredit = $trs_kredit_arr[$index];
                $trs_tipe_arus_kas = $trs_tipe_arus_kas_arr[$index];
                $trs_catatan = $trs_catatan_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'trs_field_value' => $trs_field_value,
                    'trs_field_name' => $trs_field_name,
                    'trs_table_name' => $trs_table_name,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $trs_tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => date('Y-m-d H:i:s'),
                    'trs_setor' => 0,
                    'tgl_transaksi' => $jmu_tanggal
                ];

                mAcTransaksi::create($data_transaksi);
            }
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Edit Jurnal Umum yang juga mendapatkan list transaksi pada jurnal umum tersebut
     *
     * @param Request $request
     * @return array
     */
    function edit(Request $request)
    {
        $rules = [
            'jurnal_umum_id' => [
                'required',
                'integer',
                new rJurnalUmumId()
            ]
        ];

        $attributes = [
            'master_id' => 'ID Master'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $jurnal_umum_id = $request->input('jurnal_umum_id');
        $balance_status = TRUE;
        $balance_selisih = 0;

        $data_jurnal_umum = mAcJurnalUmum
            ::with([
                'transaksi' => function ($query) {
                    $query->select([
                        'jurnal_umum_id','master_id', 'trs_jenis_transaksi','trs_tipe_arus_kas', 'trs_nama_rekening', 'trs_kode_rekening', 'trs_debet', 'trs_kredit', 'trs_catatan'
                    ]);
                }
            ])
            ->where('jurnal_umum_id', $jurnal_umum_id)
            ->select(['jurnal_umum_id', 'jmu_tanggal', 'no_invoice', 'jmu_keterangan'])
            ->orderBy('jurnal_umum_id', 'ASC')
            ->get();

        foreach ($data_jurnal_umum as $key => $row) {
            $balance_status = TRUE;
            $balance_selisih = 0;
            $total_debet = 0;
            $total_kredit = 0;
            foreach ($row->transaksi as $key_1 => $row_1) {
                if ($row_1->trs_jenis_transaksi == 'debet') {
                    $total_debet += $row_1->trs_debet;
                }
                if ($row_1->trs_jenis_transaksi == 'kredit') {
                    $total_kredit += $row_1->trs_kredit;
                }
            }

            if ($total_debet != $total_kredit) {
                $balance_status = FALSE;
                $balance_selisih = $total_debet > $total_kredit ? $total_debet - $total_kredit : $total_kredit - $total_debet;
            }

            $jurnal_umum[$key] = $row;
            $jurnal_umum[$key]['balance_status'] = $balance_status;
            $jurnal_umum[$key]['balance_selisih'] = $balance_selisih;
        }

        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();
        $total_debet = mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->sum('trs_debet');
        $total_kredit = mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->sum('trs_kredit');
        if ($total_debet != $total_kredit) {
            $balance_status = FALSE;
            $balance_selisih = $total_debet > $total_kredit ? $total_debet - $total_kredit : $total_kredit - $total_debet;
        }

        $data = [
            'jurnal_umum' => $jurnal_umum,
            'kode_perkiraan_option_list' => $kode_perkiraan_option_list,
            'total_debet' => $total_debet,
            'total_kredit' => $total_kredit,
            'balance_status' => $balance_status,
            'balance_selisih' => $balance_selisih
        ];

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Saat membuat jurnal umum ada data yang diperlukan, seperti:
     * 1. Data Input Jurnal Umum
     * 2. Data Input Transaksi untuk Jurnal Umum
     *
     * Notes :
     * Field jmu_tipe_input ini berisikan tipe data enum dengan value
     * jurnal_umum & menu_lain
     * 1. jika value jurnal_umum : tidak perlu mengisikan value pada :
     *         jmu_field_value, jmu_field_name, jmu_table_name, trs_field_value, trs_field_name dan trs_table_name
     * 2. jika value menu_lain : harus mengisikan value pada :
     *         jmu_field_value, jmu_field_name, jmu_table_name, trs_field_value, trs_field_name dan trs_table_name
     *    karena data yang masuk ke jurnal umum dan transaksi
     *    berasal dari menu-menu yang berkaitan dengan akunting, seperti menu produksi, penjualan, pembelian dan lainnya
     *    yang akan langsung masuk ke jurnal umum, sehingga diperlukan table_id dan table_name dari asal mana data
     *    untuk jurnal umum itu dimasukkan.
     *    Misaalnya: pada menu pembelian, ada transaksi akunting
     *       sehingga data pada jurnal umum
     *          jmu_field_value = 1 (increment dari primary_key bersangkutan)
     *          jmu_field_name = id_pembelian (nama field primary_key)
     *          jmu_table_name = tb_pembelian (nama table)
     *
     *          trs_field_value = 24 (increment dari primary_key bersangkutan)
     *          trs_field_name = id_pembelian_detail (nama field primary_key)
     *          trs_table_name = tb_pembelian_detail (nama table)
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $jmu_tipe_input = $request->input('jmu_tipe_input');
        $rules = [
            'jurnal_umum_id' => [
                'required',
                'integer',
                new rJurnalUmumId()
            ],
            'user_id' => 'required',
            'user_name' => 'required',
            'user_table_name' => 'required',
            'jmu_tanggal' => 'required|date_format:Y-m-d',
            'jmu_keterangan' => 'required',
            'no_invoice' => 'required',
            'jmu_tipe_input' => 'required|in:jurnal_umum,menu_lain',
            'master_id' => [
                'required',
                'array',
                new rMasterId()
            ],
            'master_id.*' => 'required',
            'trs_jenis_tranksasi.*' => 'required|in:debet,kredit',
            'trs_tipe_arus_kas.*' => 'required',
            'trs_catatan.*' => 'required',
            'trs_debet' => 'required|array',
            'trs_debet.*' => 'required',
            'trs_kredit.*' => 'required',
            'trs_kredit' => [
                'required',
                'array',
                new rJurnalUmumBalance($request->input('trs_debet'))
            ],
        ];

        if ($jmu_tipe_input == 'menu_lain') {
            $rules['jmu_field_value'] = 'required';
            $rules['jmu_field_name'] = 'required';
            $rules['jmu_table_name'] = 'required';

            $rules['trs_field_value'] = 'required|array';
            $rules['trs_field_value.*'] = 'required';
            $rules['trs_field_name'] = 'required|array';
            $rules['trs_field_name.*'] = 'required';
            $rules['trs_table_name'] = 'required|array';
            $rules['trs_table_name.*'] = 'required';
        }


        $attributes = [
            'user_id' => 'ID User',
            'user_name' => 'Nama User',
            'user_table_name' => 'Nama Table User',
            'jmu_tanggal' => 'Tanggal Jurnal Umum',
            'jmu_keterangan' => 'Keterangan',
            'no_invoice' => 'No Invoice',
            'jurnal_umum_tipe' => 'Tipe Jurnal Umum',
            'master_id' => 'ID COA',
            'trs_jenis_transaksi' => 'Jenis Transaksi',
            'trs_tipe_arus_kas' => 'Tipe Arus Kas',
            'trs_catatan' => 'Catatan',
            'trs_debet' => 'Nominal Debet',
            'trs_kredit' => 'Nominal Kredit'
        ];

        if ($jmu_tipe_input == 'menu_lain') {
            $attributes['jmu_field_value'] = 'Value Field Jurnal Umum';
            $attributes['jmu_field_name'] = 'Nama Field Jurnal Umum';
            $attributes['jmu_table_name'] = 'Nama Tabel Junrnal Umum';

            $attributes['trs_field_value'] = 'Value Field Transaksi';
            $attributes['trs_field_name'] = 'Nama Field  Transaksi';
            $attributes['trs_table_name'] = 'Nama Tabel  Transaksi';
        }


        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['master_id.' . $i] = 'Kode Perkiraan ke-' . $next;
            $attr['trs_jenis_transaksi.' . $i] = 'Jenis Transaksi ke-' . $next;
            $attr['trs_tipe_arus_kas.' . $i] = 'Tipe Arus Kas ke-' . $next;
            $attr['trs_catatan.' . $i] = 'Catatan ke-' . $next;
            $attr['trs_debet.' . $i] = 'Nominal Debet ke-' . $next;
            $attr['trs_kredit.' . $i] = 'Nominal Kredit ke-' . $next;

            if ($jmu_tipe_input == 'menu_lain') {
                $attr['trs_field_value.' . $i] = 'Value Field Transaksi Database ke-' . $next;
                $attr['trs_field_name.' . $i] = 'Nama Field Transaksi Database ke-' . $next;
                $attr['trs_table_name.' . $i] = 'Nama Tabel Transaksi Database ke-' . $next;
            }
        }


        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $user_id = $request->input('user_id');
            $user_name = $request->input('user_name');
            $user_table_name = $request->input('user_table_name');
            $jurnal_umum_id = $request->input('jurnal_umum_id');
            $jmu_tanggal = $request->input('jmu_tanggal');
            $no_invoice = $request->input('no_invoice');
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);
            $jmu_keterangan = $request->input('jmu_keterangan');
            $jmu_field_value = $request->input('jmu_field_value');
            $jmu_field_name = $request->input('jmu_field_name');
            $jmu_table_name = $request->input('jmu_table_name');

            $master_id_arr = $request->input('master_id');
            $trs_field_value_arr = $request->input('trs_field_value');
            $trs_field_name_arr = $request->input('trs_field_name');
            $trs_table_name_arr = $request->input('trs_table_name');
            $trs_jenis_transaksi_arr = $request->input('trs_jenis_transaksi');
            $trs_debet_arr = $request->input('trs_debet');
            $trs_kredit_arr = $request->input('trs_kredit');
            $trs_tipe_arus_kas_arr = $request->input('trs_tipe_arus_kas');
            $trs_catatan_arr = $request->input('trs_catatan');

            $data_jurnal_umum = [
                'user_id' => $user_id,
                'user_name' => $user_name,
                'user_table_name' => $user_table_name,
                'no_invoice' => $no_invoice,
                'jmu_tanggal' => $jmu_tanggal,
                'jmu_tipe_input' => $jmu_tipe_input,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => $jmu_keterangan,
            ];

            if ($jmu_tipe_input == 'menu_lain') {
                $data_jurnal_umum['jmu_field_value'] = $jmu_field_value;
                $data_jurnal_umum['jmu_field_name'] = $jmu_field_name;
                $data_jurnal_umum['jmu_table_name'] = $jmu_table_name;
            }

            mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->update($data_jurnal_umum);
            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

            foreach ($master_id_arr as $index => $master_id) {
                $master = mAcMaster
                    ::where('master_id', $master_id)
                    ->first([
                        'mst_kode_rekening',
                        'mst_nama_rekening'
                    ]);
                $trs_field_value = $trs_field_value_arr[$index];
                $trs_field_name = $trs_field_name_arr[$index];
                $trs_table_name = $trs_table_name_arr[$index];
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
                $trs_jenis_transaksi = $trs_jenis_transaksi_arr[$index];
                $trs_debet = $trs_debet_arr[$index];
                $trs_kredit = $trs_kredit_arr[$index];
                $trs_tipe_arus_kas = $trs_tipe_arus_kas_arr[$index];
                $trs_catatan = $trs_catatan_arr[$index];

                $data_transaksi = [
                    'jurnal_umum_id' => $jurnal_umum_id,
                    'trs_field_value' => $trs_field_value,
                    'trs_field_name' => $trs_field_name,
                    'trs_table_name' => $trs_table_name,
                    'master_id' => $master_id,
                    'trs_jenis_transaksi' => $trs_jenis_transaksi,
                    'trs_debet' => $trs_debet,
                    'trs_kredit' => $trs_kredit,
                    'trs_year' => $year,
                    'trs_month' => $month,
                    'trs_kode_rekening' => $trs_kode_rekening,
                    'trs_nama_rekening' => $trs_nama_rekening,
                    'trs_tipe_arus_kas' => $trs_tipe_arus_kas,
                    'trs_catatan' => $trs_catatan,
                    'trs_charge' => 0,
                    'trs_no_check_bg' => '',
                    'trs_tgl_pencairan' => date('Y-m-d H:i:s'),
                    'trs_setor' => 0,
                    'tgl_transaksi' => $jmu_tanggal
                ];

                mAcTransaksi::create($data_transaksi);
            }
            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete jurnal umum beserta transaksinya
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'jurnal_umum_id' => [
                'required',
                'integer',
                new rJurnalUmumId()
            ]
        ];

        $attributes = [
            'master_id' => 'ID Master'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $jurnal_umum_id = $request->input('jurnal_umum_id');

        mAcJurnalUmum::where(['jurnal_umum_id' => $jurnal_umum_id])->delete();
        mAcTransaksi::where(['jurnal_umum_id' => $jurnal_umum_id])->delete();
        return General::response(General::$success, General::$delete);

    }


}


