<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcAsset;
use App\Models\mAcAssetKategori;
use App\Models\mAcAssetPenyusutan;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rIdAsset;
use App\Rules\rIdAssetKategori;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Asset extends Controller
{

    private $datetime;
    private $attributes = [
        'id_asset_kategori' => 'ID Asset Kategori',
        'nama' => 'Nama Asset',
        'tanggal_beli' => 'Tanggal Beli Asset',
        'qty' => 'Qty Asset',
        'lokasi' => 'Lokasi',
        'metode' => 'Metode Perhitungan',
        'tanggal_pensiun' => 'Tanggal Pensiun',
        'terhitung_tanggal' => 'Terhitung Tanggal',
        'nilai_buku' => 'Nilai Buku',
        'master_id_asset' => 'ID COA Asset',
        'master_id_akumulasi' => 'ID COA Akumulasi',
        'master_id_depresiasi' => 'ID COA Depresiasi',
        'master_id_jenis_pembayaran' => 'ID COA Jenis Pembayaran',
    ];

    public function __construct()
    {
        $this->datetime = date('Y-m-d H:i:s');
    }

    /**
     * Get data asset list
     *
     * @return array
     */
    function index()
    {

        $data = mAcAsset
            ::with('asset_kategori:id,nama')
            ->orderBy('id', 'ASC')
            ->get();
        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Keperluan saat membuat create asset
     * 1. Kode Asset : untuk kode asset pada barang yang akan disimpan
     * 2. Asset Kategori : untuk data kategori asset saat pembuatan asset baru
     * 3. Kode Perkiraan : saat pembuatan asset, digunakan saat pembuatan asset baru
     * @return array
     */
    function create()
    {
        $asset_kategori = mAcAssetKategori::orderBy('nama')->get();
        $kode_asset = hAkunting::kode_asset();
        $kode_perkiraan_option_list = hAkunting::kode_perkiraan_select_list();

        $data = [
            'kode_asset' => $kode_asset,
            'asset_kategori' => $asset_kategori,
            'kode_perkiraan_option_list' => $kode_perkiraan_option_list
        ];

        return General::response(General::$success, General::$get, $data);

    }

    /**
     * Untuk membuat data asset
     *
     * kode asset didapatkan dari API asset/create
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'id_asset_kategori' => [
                'required',
                new rIdAssetKategori()
            ],
        ];

        General::validator($request->all(), $rules, [], $this->attributes);


        $id_asset_kategori = $request->input('id_asset_kategori');
        $penyusutan_status = mAcAssetKategori::where('id', $id_asset_kategori)->value('penyusutan_status');
        if ($penyusutan_status == 'yes') {
            $rules = [
                'kode_asset' => 'required',
                'nama_asset' => 'required',
                'tanggal_beli' => 'required|date_format:Y-m-d',
                'qty' => 'required|numeric',
                'harga_beli' => 'required|numeric',
                'total_beli' => 'required|numeric',
                'lokasi' => 'required',
                'metode' => 'required',
                'tanggal_pensiun' => 'required|date_format:Y-m-d',
                'akumulasi_beban' => 'required|numeric',
                'terhitung_tanggal' => 'required|date_format:Y-m-d',
                'nilai_buku' => 'required|numeric',
                'beban_perbulan' => 'required|numeric',
                'master_id_asset' => [
                    'required',
                    new rMasterId()
                ],
                'master_id_akumulasi' => [
                    'required',
                    new rMasterId()
                ],
                //'master_id_depresiasi' => 'required',
                'master_id_jenis_pembayaran' => [
                    'required',
                    new rMasterId()
                ],
            ];
        } else {
            $rules = [
                'kode_asset' => 'required',
                'nama_asset' => 'required',
                'tanggal_beli' => 'required|date_format:Y-m-d',
                'qty' => 'required|numeric',
                'harga_beli' => 'required|numeric',
                'total_beli' => 'required|numeric',
                'lokasi' => 'required',
                'metode' => 'required',
                'tanggal_pensiun' => 'required|date_format:Y-m-d',
                //'akumulasi_beban' => 'required',
                'terhitung_tanggal' => 'required|date_format:Y-m-d',
                'nilai_buku' => 'required|numeric',
                //'beban_perbulan' => 'required',
                'master_id_asset' => [
                    'required',
                    new rMasterId()
                ],
                'master_id_akumulasi' => [
                    'required',
                    new rMasterId()
                ],
                'master_id_depresiasi' => [
                    'required',
                    new rMasterId()
                ],
                'master_id_jenis_pembayaran' => [
                    'required',
                    new rMasterId()
                ],
            ];
        }

        General::validator($request->all(), $rules, [], $this->attributes);

        DB::beginTransaction();
        try {
            $kode_asset = $request->input('kode_asset');
            $nama = strtoupper($request->input('nama_asset'));
            $tanggal_beli = $request->input('tanggal_beli');
            $tanggal_beli_2 = $tanggal_beli;
            $qty = $request->input('qty');
            $harga_beli = $request->input('harga_beli');
            $provisi = $request->input('provisi');
            $total_beli = $request->input('total_beli');
            $nilai_residu = $request->input('nilai_residu');
            $umur_ekonomis = $request->input('umur_ekonomis');
            $lokasi = strtoupper($request->input('lokasi'));
            $tanggal_cek = $request->input('tanggal_cek');
            $metode = $request->input('metode');
            $tanggal_pensiun = $request->input('tanggal_pensiun');
            $akumulasi_beban = $request->input('akumulasi_beban');
            $terhitung_tanggal = $request->input('terhitung_tanggal');
            $nilai_buku = $request->input('nilai_buku');
            $beban_perbulan = $request->input('beban_perbulan');
            $master_id_asset = $request->input('master_id_asset');
            $master_id_akumulasi = $request->input('master_id_akumulasi');
            $master_id_depresiasi = $request->input('master_id_depresiasi');
            $master_id_jenis_pembayaran = $request->input('master_id_jenis_pembayaran');
//            $asset_profisi = $request->input('asset_profisi');
            $harga_beli_2 = 0;

            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $jmu_no = hAkunting::jmu_no($year, $month, $day);


            $data_asset = [
                'kode_asset' => $kode_asset,
                'nama' => $nama,
                'id_asset_kategori' => $id_asset_kategori,
                'tanggal_beli' => $tanggal_beli,
                'tanggal_beli_2' => $tanggal_beli_2,
                'qty' => $qty,
                'harga_beli' => $harga_beli,
                'total_beli' => $total_beli,
                'nilai_residu' => $nilai_residu,
                'umur_ekonomis' => $umur_ekonomis,
                'lokasi' => $lokasi,
                'akumulasi_beban' => $akumulasi_beban,
                'terhitung_tanggal' => $terhitung_tanggal,
                'nilai_buku' => $nilai_buku,
                'provisi' => $provisi,
                'beban_perbulan' => $beban_perbulan,
                'metode' => $metode,
                'tanggal_pensiun' => $tanggal_pensiun,
                'master_id_asset' => $master_id_asset,
                'master_id_akumulasi' => $master_id_akumulasi,
                'master_id_depresiasi' => $master_id_depresiasi,
                'master_id_jenis_pembayaran' => $master_id_jenis_pembayaran
            ];

            $response = mAcAsset::create($data_asset);
            $id_asset = $response->id;

            $data_jurnal_umum = [
                'table_id' => json_encode([$id_asset]),
                'table_name' => json_encode(['tb_ac_asset']),
                'no_invoice' => $kode_asset,
                'jmu_tanggal' => $this->datetime,
                'jmu_no' => $jmu_no,
                'jmu_year' => $year,
                'jmu_month' => $month,
                'jmu_day' => $day,
                'jmu_keterangan' => 'Pengadaan Asset No : ' . $kode_asset,
            ];

            $response = mAcJurnalUmum::create($data_jurnal_umum);
            $jurnal_umum_id = $response->jurnal_umum_id;

            $master = mAcMaster::where('master_id', $master_id_asset)->first();
            $trs_kode_rekening = $master->mst_kode_rekening;
            $trs_nama_rekening = $master->mst_nama_rekening;

            $debet = $harga_beli_2;

            $data_transaksi_piutang = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_asset,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet' => $debet,
                'trs_kredit' => 0,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Investasi',
                'trs_catatan' => 'Pengadaan Asset',
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => $this->datetime
            ];
            mAcTransaksi::create($data_transaksi_piutang);

            $master_jenis_pembayaran = mAcMaster::where('master_id', $master_id_jenis_pembayaran)->first(['mst_kode_rekening', 'mst_nama_rekening']);
            $trs_kode_rekening = $master_jenis_pembayaran->mst_kode_rekening;
            $trs_nama_rekening = $master_jenis_pembayaran->mst_nama_rekening;
            $jns_pembayaran = [
                'jurnal_umum_id' => $jurnal_umum_id,
                'master_id' => $master_id_jenis_pembayaran,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet' => 0,
                'trs_kredit' => $harga_beli,
                'trs_year' => $year,
                'trs_month' => $month,
                'trs_kode_rekening' => $trs_kode_rekening,
                'trs_nama_rekening' => $trs_nama_rekening,
                'trs_tipe_arus_kas' => 'Investasi',
                'trs_catatan' => 'Pengadaan Asset',
                'trs_charge' => 0,
                'trs_no_check_bg' => 0,
                'trs_tgl_pencairan' => $this->datetime,
                'trs_setor' => 0,
                'tgl_transaksi' => $this->datetime
            ];
            mAcTransaksi::create($jns_pembayaran);

            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$store);
        }
    }

    /**
     * untuk mendapatkan detail dari asset per row
     *
     * @param Request $request
     * @return array
     */
    function detail(Request $request)
    {
        $rules = [
            'id_asset' => [
                'required',
                new rIdAsset()
            ],
        ];

        $attributes = [
            'id_asset' => 'ID Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $id_asset = $request->input('id_asset');
        $data = mAcAsset
            ::with([
                'asset_kategori',
                'master_asset',
                'master_akumulasi',
//                'master_depreasi',
                'master_jenis_pembayaran',
                'penyusutan_asset'
            ])
            ->where('id', $id_asset)
            ->first();

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Delete asset juga mendelete data di asset, asset penyusutan, jurnal umum dan transaksi
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    function delete(Request $request)
    {
        $rules = [
            'id_asset' => [
                'required',
                new rIdAsset()
            ],
        ];

        $attributes = [
            'id_asset' => 'ID Asset',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $id_asset = $request->input('id_asset');
        DB::beginTransaction();
        try {
            $table_id = json_encode([$id_asset]);
            $table_name = json_encode(['tb_ac_asset']);
            $where = [
                'table_id' => $table_id,
                'table_name' => $table_name
            ];
            $jurnal_umum_id = mAcJurnalUmum::where($where)->value('jurnal_umum_id');

            mAcAsset::where('id', $id_asset)->delete();
            mAcAssetPenyusutan::where('id_asset', $id_asset)->delete();
            mAcJurnalUmum::where($where)->delete();
            mAcTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->delete();

            DB::commit();

        } catch (\Exception $exception) {
            throw $exception;

            DB::rollback();
        }

        return General::response(General::$success, General::$delete);
    }


}


