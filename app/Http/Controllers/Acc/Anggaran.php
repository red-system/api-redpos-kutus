<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcAnggaran;
use App\Models\mAcAnggaranDetail;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rAnggaranIdExist;
use App\Rules\rAnggaranPerkiraanNominal;
use App\Rules\rAnggaranTahun;
use App\Rules\rAnggaranTahunEdit;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Anggaran extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Anggaran
     *
     * @param Request $request
     * @return array
     */
    function list(Request $request)
    {
        $data = mAcAnggaran
            ::select(['anggaran_id','anggaran_tahun','created_at','updated_at'])
            ->withCount([
                'anggaran_detail AS anggaran_total' => function ($query) {
                    $query->select(DB::raw('SUM(anggaran_nominal) AS anggaran_total'));
                }
            ])
            ->orderBy('anggaran_tahun', 'DESC')
            ->get();
        foreach ($data as $row) {
            $row->realisasi_anggaran_total = mAcTransaksi::whereYear('tgl_transaksi', $row->anggaran_tahun)->sum('trs_kredit');
        }

        return General::response(General::$success, General::$get, $data);
    }

    /**
     *
     * Membuat Anggaran Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'anggaran_tahun' => [
                'required',
                new rAnggaranTahun()
            ],
            'anggaran' => [
                'required',
                'array',
                new rAnggaranPerkiraanNominal()
            ],
            'user_table_name' => 'required',
            'user_id_created' => 'required',
            'user_name_created' => 'required',
        ];


        $attributes = [
            'anggaran_tahun' => 'Tahun Anggaran',
            'anggaran' => 'Anggaran Perkiraan dan Nominal',
            'user_table_name' => 'Nama Table User',
            'user_id_created' => 'ID User Creator',
            'user_name_created' => 'User Name Creator',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $user_id_created = $request->input('user_id_created');
            $user_name_created = $request->input('user_name_created');
            $user_table_name = $request->input('user_table_name');
            $anggaran_tahun = $request->input('anggaran_tahun');
            $anggaran = $request->input('anggaran');


            $data_anggaran = [
                'anggaran_tahun' => $anggaran_tahun,
                'user_table_name' => $user_table_name,
                'user_id_created' => $user_id_created,
                'user_name_created' => $user_name_created,
            ];

            $response = mAcAnggaran::create($data_anggaran);
            $anggaran_id = $response->anggaran_id;

            foreach ($anggaran as $row) {
                $data_anggaran_detail = [
                    'anggaran_id' => $anggaran_id,
                    'master_id' => $row['master_id'],
                    'anggaran_nominal' => $row['nominal']
                ];

                mAcAnggaranDetail::create($data_anggaran_detail);
            }


            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Edit Anggaran
     *
     * @param Request $request
     * @return array
     */
    function edit(Request $request)
    {
        $rules = [
            'anggaran_id' => [
                'required',
                'integer',
                new rAnggaranIdExist()
            ]
        ];

        $attributes = [
            'anggaran_id' => 'ID Anggaran'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $anggaran_id = $request->input('anggaran_id');
        $data = mAcAnggaran
            ::select(['anggaran_id','anggaran_tahun','created_at','updated_at'])
            ->with([
                'anggaran_detail' => function ($query) {
                    $query->select(['anggaran_id', 'master_id', 'anggaran_nominal'])->orderBy('master_id', 'ASC');
                },
                'anggaran_detail.master' => function ($query) {
                    $query->select(['master_id', 'mst_kode_rekening', 'mst_nama_rekening']);
                }
            ])
            ->withCount([
                'anggaran_detail AS anggaran_total' => function ($query) {
                    $query->select(DB::raw('SUM(anggaran_nominal) AS anggaran_total'));
                }
            ])
            ->where('anggaran_id', $anggaran_id)
            ->first();

        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Update Anggaran
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'anggaran_id' => [
                'required',
                'integer',
                new rAnggaranIdExist()
            ],
            'anggaran_tahun' => [
                'required',
                new rAnggaranTahunEdit($request->anggaran_id)
            ],
            'anggaran' => [
                'required',
                'array',
                new rAnggaranPerkiraanNominal()
            ],
            'user_id_updated' => 'required',
            'user_name_updated' => 'required',
        ];


        $attributes = [
            'anggaran_id' => 'ID Anggaran',
            'anggaran_tahun' => 'Tahun Anggaran',
            'anggaran' => 'Anggaran Perkiraan dan Nominal',
            'user_id_updated' => 'ID User Updator',
            'user_name_updated' => 'User Name Updator',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {

            $anggaran_id = $request->input('anggaran_id');
            $anggaran_tahun = $request->input('anggaran_tahun');
            $user_id_updated = $request->input('user_id_updated');
            $user_name_updated = $request->input('user_name_updated');
            $anggaran = $request->input('anggaran');

            $data_anggaran = [
                'anggaran_Tahun' => $anggaran_tahun,
                'user_id_updated' => $user_id_updated,
                'user_name_updated' => $user_name_updated,
            ];

            mAcAnggaran::where('anggaran_id', $anggaran_id)->update($data_anggaran);
            mAcAnggaranDetail::where('anggaran_id', $anggaran_id)->delete();
            foreach ($anggaran as $row) {
                $data_anggaran_detail = [
                    'anggaran_id' => $anggaran_id,
                    'master_id' => $row['master_id'],
                    'anggaran_nominal' => $row['nominal']
                ];

                mAcAnggaranDetail::create($data_anggaran_detail);
            }


            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Anggaran
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'anggaran_id' => [
                'required',
                'integer',
                new rAnggaranIdExist()
            ]
        ];

        $attributes = [
            'anggaran_id' => 'ID Anggaran'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $anggaran_id = $request->input('anggaran_id');

        mAcAnggaran::where(['anggaran_id' => $anggaran_id])->delete();
        mAcAnggaranDetail::where('anggaran_id', $anggaran_id)->delete();
        return General::response(General::$success, General::$delete);

    }

    /**
     * Detail Realisasi Anggaran
     */
    function detail(Request $request)
    {
        $rules = [
            'anggaran_id' => [
                'required',
                'integer',
                new rAnggaranIdExist()
            ]
        ];

        $attributes = [
            'anggaran_id' => 'ID Anggaran'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $anggaran_id = $request->input('anggaran_id');
        $realisasi_anggaran_total = 0;
        $data = mAcAnggaran
            ::select(['anggaran_id','anggaran_tahun','created_at','updated_at'])
            ->with([
                'anggaran_detail' => function ($query) {
                    $query->select(['anggaran_id', 'master_id', 'anggaran_nominal'])->orderBy('master_id', 'ASC');
                },
                'anggaran_detail.master' => function ($query) {
                    $query->select(['master_id', 'mst_kode_rekening', 'mst_nama_rekening']);
                }
            ])
            ->withCount([
                'anggaran_detail AS anggaran_total' => function ($query) {
                    $query->select(DB::raw('SUM(anggaran_nominal) AS anggaran_total'));
                }
            ])
            ->where('anggaran_id', $anggaran_id)
            ->first();

        foreach($data->anggaran_detail as $row_1) {
            $row_1->realisasi_anggaran_total = mAcTransaksi::whereYear('tgl_transaksi', $data->anggaran_tahun)->where('master_id', $row_1['master']['master_id'])->sum('trs_kredit');
            $realisasi_anggaran_total += $row_1->realisasi_anggaran_total;

            $row_1->budget_status = $row_1->realisasi_anggaran_total > $row_1->anggaran_total ? 'Over Budget' : 'On Budget';
        }

        $data->realisasi_anggaran_total = $realisasi_anggaran_total;
        $data->budget_status = $realisasi_anggaran_total > $data->anggaran_total ? 'Over Budget' : 'On Budget';

        return General::response(General::$success, General::$get, $data);
    }


}


