<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Helpers\Main;
use App\Http\Controllers\Controller;
use App\Models\mAcJurnalUmum;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;
use App\Rules\rJurnalUmumBalance;
use App\Rules\rJurnalUmumId;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LabaRugi extends Controller
{

    public function __construct()
    {

    }

    /**
     * Keperluan data untuk laba rugi dengann proses kalkulasi yang sangat "mudah"
     * dengan input data date_start dan date_end
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $rules = [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
        ];

        $attributes = [
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');
        $data = [];
        $where_date = [
            $date_start,
            $date_end
        ];

        /**
         * Begin Transaksi Pendapatan
         */
        $pendapatan_data = mAcMaster
            ::with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
                'childs.childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },

                'transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.childs.childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                }
            ])
            ->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening'])
            ->where('mst_kode_rekening', '3100')
            ->get();

        $pendapatan_total = 0;
        foreach ($pendapatan_data as $row_1) {
            $total_row_1 = 0;
            foreach ($row_1->childs as $row_2) {
                $total_row_2 = 0;
                foreach ($row_2->childs as $row_3) {
                    $total_row_3 = 0;
                    foreach ($row_3->childs as $row_4) {

                        if ($row_4->mst_normal == 'kredit') {

                            $total_debet = $row_4->transaksi->sum('trs_debet');
                            $total_kredit = $row_4->transaksi->sum('trs_kredit');

                            $total_kalkulasi = $total_kredit - $total_debet;
                            $total_row_3 += $total_kalkulasi;

                            $row_4['value'] = $total_kalkulasi;
                        }

                        if ($row_4->mst_normal == 'debet') {

                            $total_debet = $row_4->transaksi->sum('trs_debet');
                            $total_kredit = $row_4->transaksi->sum('trs_kredit');

                            $total_kalkulasi = $total_debet - $total_kredit;
                            $total_row_3 -= $total_kalkulasi;

                            $row_4['value'] = $total_kalkulasi;
                        }
                    }

                    if ($row_3->mst_normal == 'kredit') {

                        $total_kredit = $row_3
                            ->transaksi
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_kredit');

                        $total_kalkulasi = $total_row_3 + $total_kredit;
                        $total_row_2 += $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }

                    if ($row_3->mst_normal == 'debet') {

                        $total_debet = $row_3
                            ->transaksi
                            ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                            ->sum('trs_debet');

                        $total_kalkulasi = $total_row_3 - $total_debet;
                        $total_row_2 -= $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }
                }

                if ($row_2->mst_normal == 'kredit') {
                    $total_kredit = $row_2
                        ->transaksi
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_kredit');

                    $total_kalkulasi = $total_row_2 + $total_kredit;
                    $total_row_1 += $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }
                if ($row_2->mst_normal == 'debet') {
                    $total_debet = $row_2
                        ->transaksi
                        ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                        ->sum('trs_debet');

                    $total_kalkulasi = $total_row_2 - $total_debet;
                    $total_row_1 -= $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }
            }

            if ($row_1->mst_normal == 'kredit') {
                $total_kredit = $row_1
                    ->transaksi
                    ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                    ->sum('trs_kredit');

                $total_kalkulasi = $total_row_1 + $total_kredit;
                $row_1['value'] = $total_kalkulasi;
            }

            if ($row_1->mst_normal == 'debet') {

                $total_debet = $row_1
                    ->transaksi
                    ->where('trs_tipe_arus_kas', '!=', 'Saldo Awal')
                    ->sum('trs_debet');

                $total_kalkulasi = $total_row_1 - $total_debet;
                $row_1['value'] = $total_kalkulasi;
            }

            $pendapatan_total += $total_kalkulasi;
        }
        /**
         * End Transaksi Pendapatan
         */

        /**
         * Begin Transaksi HPP
         */
        $hpp_data = mAcMaster
            ::with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
//                'childs.childs.childs' => function ($query) {
//                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
//                }



                'transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
//                'childs.childs.childs.transaksi' => function ($query) use ($where_date) {
//                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
//                        ->whereBetween('tgl_transaksi', $where_date);
//                }
            ])
            ->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening'])
            ->where('mst_kode_rekening', '4100')
            ->get();
        $hpp_total = 0;
        foreach ($hpp_data as $row_1) {
            $total_row_1 = 0;
            foreach ($row_1->childs as $row_2) {
                $total_row_2 = 0;
                foreach ($row_2->childs as $row_3) {
                    if ($row_3->mst_normal == 'kredit') {

                        $total_kredit = $row_3->transaksi->sum('trs_kredit');
                        $total_debet = $row_3->transaksi->sum('trs_debet');

                        $total_kalkulasi = $total_kredit - $total_debet;
                        $total_row_2 -= $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }
                    if ($row_2->mst_normal == 'debet') {

                        $total_kredit = $row_3->transaksi->sum('trs_kredit');
                        $total_debet = $row_3->transaksi->sum('trs_debet');

                        $total_kalkulasi = $total_debet - $total_kredit;
                        $total_row_2 += $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }
                }

                if ($row_2->mst_normal == 'kredit') {
                    $total_kredit = $row_2->transaksi->sum('trs_kredit');

                    $total_kalkulasi = $total_row_2 - $total_kredit;
                    $total_row_1 -= $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }

                if ($row_2->mst_normal == 'debet') {
                    $total_debet = $row_2->transaksi->sum('trs_debet');

                    $total_kalkulasi = $total_row_2 + $total_debet;
                    $total_row_1 += $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }
            }

            if ($row_1->mst_normal == 'kredit') {
                $total_kredit = $row_1->transaksi->sum('trs_kredit');

                $total_kalkulasi = $total_row_1 - $total_kredit;
                $row_1['value'] = $total_kalkulasi;
            }

            if ($row_1->mst_normal == 'debet') {
                $total_debet = $row_1->transaksi->sum('trs_debet');

                $total_kalkulasi = $total_row_1 + $total_debet;
                $row_1['value'] = $total_kalkulasi;
            }

            $hpp_total += $total_kalkulasi;
        }
        /**
         * End Transaksi HPP
         */

        /**
         * Begin Biaya
         */
        $biaya_data = mAcMaster
            ::with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
                'childs.childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
//                'childs.childs.childs' => function ($query) {
//                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
//                }


                'transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
            ])
            ->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening'])
            ->where('mst_kode_rekening', '5100')
            ->get();
        $biaya_total = 0;
        foreach ($biaya_data as $row_1) {
            $total_row_1 = 0;
            foreach ($row_1->childs as $row_2) {
                $total_row_2 = 0;
                foreach ($row_2->childs as $row_3) {
                    if ($row_3->mst_normal == 'kredit') {

                        $total_kredit = $row_3->transaksi->sum('trs_kredit');
                        $total_debet = $row_3->transaksi->sum('trs_debet');

                        $total_kalkulasi = $total_kredit - $total_debet;
                        $total_row_2 -= $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }

                    if ($row_3->mst_normal == 'debet') {

                        $total_kredit = $row_3->transaksi->sum('trs_kredit');
                        $total_debet = $row_3->transaksi->sum('trs_debet');

                        $total_kalkulasi = $total_debet - $total_kredit;
                        $total_row_2 += $total_kalkulasi;

                        $row_3['value'] = $total_kalkulasi;
                    }
                }

                if ($row_2->mst_normal == 'kredit') {

                    $total_kredit = $row_2->transaksi->sum('trs_kredit');
                    $total_debet = $row_2->transaksi->sum('trs_debet');

                    $total_kalkulasi = $total_row_2 - $total_kredit - $total_debet;
                    $total_row_1 -= $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }

                if ($row_2->mst_normal == 'debet') {

                    $total_kredit = $row_2->transaksi->sum('trs_kredit');
                    $total_debet = $row_2->transaksi->sum('trs_debet');

                    $total_kalkulasi = $total_row_2 + $total_debet - $total_kredit;
                    $total_row_1 += $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }
            }

            if ($row_1->mst_normal == 'kredit') {

                $total_kredit = $row_1->transaksi->sum('trs_kredit');
                $total_debet = $row_1->transaksi->sum('trs_debet');

                $total_kalkulasi = $total_row_1 - $total_kredit - $total_debet;
                $row_1['value'] = $total_kalkulasi;
            }

            if ($row_1->mst_normal == 'debet') {

                $total_kredit = $row_1->transaksi->sum('trs_kredit');
                $total_debet = $row_1->transaksi->sum('trs_debet');

                $total_kalkulasi = $total_row_1 + $total_debet - $total_kredit;
                $row_1['value'] = $total_kalkulasi;
            }

            $biaya_total += $total_kalkulasi;
        }
        /**
         * End Biaya
         */

        /**
         * Begin Transaksi Pendapatan Diluar Usaha
         */
        $pendapatan_diluar_usaha_data = mAcMaster
            ::with([
                'childs' => function ($query) {
                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
                },
//                'childs.childs' => function ($query) {
//                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
//                },
//                'childs.childs.childs' => function ($query) {
//                    $query->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening']);
//                }

                'transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
                'childs.transaksi' => function ($query) use ($where_date) {
                    $query->select(['master_id', 'trs_kredit', 'trs_debet'])
                        ->whereBetween('tgl_transaksi', $where_date);
                },
            ])
            ->select(['master_id', 'mst_master_id', 'mst_normal', 'mst_kode_rekening', 'mst_nama_rekening'])
            ->where('mst_kode_rekening', '3120')
            ->get();
        $pendapatan_diluar_usaha_total = 0;
        foreach ($pendapatan_diluar_usaha_data as $row_1) {
            $total_row_1 = 0;
            foreach ($row_1->childs as $row_2) {
                if ($row_2->mst_normal == 'kredit') {

                    $total_debet = $row_2->transaksi->sum('trs_debet');
                    $total_kredit = $row_2->transaksi->sum('trs_kredit');

                    $total_kalkulasi = $total_kredit - $total_debet;
                    $total_row_1 += $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;

                }
                if ($row_2->mst_normal == 'debet') {

                    $total_debet = $row_2->transaksi->sum('trs_debet');
                    $total_kredit = $row_2->transaksi->sum('trs_kredit');

                    $total_kalkulasi = $total_debet - $total_kredit;
                    $total_row_1 -= $total_kalkulasi;

                    $row_2['value'] = $total_kalkulasi;
                }
            }

            if ($row_1->mst_normal == 'kredit') {
                $total_kredit = $row_1->transaksi->sum('trs_kredit');

                $total_kalkulasi = $total_row_1 + $total_kredit;
                $row_1['value'] = $total_kalkulasi;
            }

            if ($row_1->mst_normal == 'debet') {
                $total_debet = $row_1->transaksi->sum('trs_debet');

                $total_kalkulasi = $total_row_1 - $total_debet;
                $row_1['value'] = $total_kalkulasi;
            }

            $pendapatan_diluar_usaha_total += $total_kalkulasi;
        }
        /**
         * End Transaksi Pendapatan Diluar Usaha
         */


        $data = [
            'laba_rugi_kotor' => [
                'label' => 'LABA (RUGI) KOTOR',
                'value' => $pendapatan_total - $hpp_total,
                'data' => [
                    [
                        'data' => $pendapatan_data,
                        'total' => $pendapatan_total
                    ],
                    [
                        'data' => $hpp_data,
                        'total' => $hpp_total
                    ]
                ]
            ],
            'laba_rugi_bersih' => [
                'label' => 'LABA (RUGI) BERSIH',
                'value' => $pendapatan_total - $hpp_total - $biaya_total + $pendapatan_diluar_usaha_total,
                'data' => [
                    [
                        'data' => $biaya_data,
                        'total' => $biaya_total
                    ],
                    [
                        'data' => $pendapatan_diluar_usaha_data,
                        'total' => $pendapatan_diluar_usaha_total
                    ]
                ]
            ]
        ];

        return General::response(General::$success, General::$get, $data);
    }


}


