<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Models\mAcMaster;
use App\Rules\rMasterId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Coa extends Controller
{

    public function __construct()
    {

    }

    /**
     * Mendapatkan list COA dengan disetiap COA mempunyai sub2 nya
     *
     * @return array
     */
    function index()
    {
        $master = mAcMaster
            ::where('mst_master_id', 0)
            ->with([
                'childs',
                'childs.childs',
                'childs.childs.childs',
            ])
            ->get();


        return General::response(General::$success, General::$get, $master);
    }

    /**
     * Membuat COA baru
     *
     * @param Request $request
     * @return array
     */
    function store(Request $request)
    {

        $rules = [
            'mst_master_id' => [
                'required',
                'integer'
            ],
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
            'mst_posisi' => 'required|in:neraca,laba rugi',
            'mst_normal' => 'required|in:debet,kredit',
            'mst_tipe_laporan' => 'required|in:laba kotor,laba bersih',
            'mst_tipe_nominal' => 'required|in:pendapatan,beban',
            'mst_neraca_tipe' => 'required|in:asset,liabilitas,ekuitas,null',
            'mst_kas_status' => 'required|in:yes,no',
            'mst_pembayaran' => 'required|in:yes,no',
        ];

        $attributes = [
            'mst_master_id' => 'ID Master Parent',
            'mst_kode_rekening' => 'Kode Rekening',
            'mst_nama_rekening' => 'Nama Rekening',
            'mst_posisi' => 'Posisi',
            'mst_normal' => 'Normal',
            'mst_tipe_laporan' => 'Tipe Laporan',
            'mst_tipe_nominal' => 'Tipe Nominal',
            'mst_neraca_tipe' => 'Tipe Neraca',
            'mst_kas_status' => 'Status Kas',
            'mst_pembayaran' => 'Pembayaran',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        DB::beginTransaction();
        try {

            $mst_master_id = $request->input('mst_master_id');
            $mst_kode_rekening = $request->input('mst_kode_rekening');
            $mst_nama_rekening = $request->input('mst_nama_rekening');
            $mst_posisi = $request->input('mst_posisi');
            $mst_normal = $request->input('mst_normal');
            $mst_tipe_laporan = $request->input('mst_tipe_laporan');
            $mst_tipe_nominal = $request->input('mst_tipe_nominal');
            $mst_neraca_tipe = $request->input('mst_neraca_tipe');
            $mst_kas_status = $request->input('mst_kas_status');
            $mst_pembayaran = $request->input('mst_pembayaran');
            $mst_tanggal_awal = date('Y-m-d');

            $data_master = [
                'mst_master_id' => $mst_master_id,
                'mst_kode_rekening' => $mst_kode_rekening ,
                'mst_nama_rekening' => $mst_nama_rekening,
                'mst_posisi' => $mst_posisi,
                'mst_normal' => $mst_normal,
                'mst_tipe_laporan' => $mst_tipe_laporan,
                'mst_tipe_nominal' => $mst_tipe_nominal,
                'mst_neraca_tipe' => $mst_neraca_tipe,
                'mst_kas_status' => $mst_kas_status,
                'mst_pembayaran' => $mst_pembayaran,
                'mst_tanggal_awal' => $mst_tanggal_awal
            ];

            $master = mAcMaster::create($data_master);

//            /**
//             * Code dibawah ini adalah cara akunta pertama kali, setiap perubahan disimpan pada master detail
//             */
//            $master_id = $master->master_id;
//            if ($mst_normal == 'debet') {
//                $msd_awal_kredit = 0;
//                $msd_awal_debet = $request->input('nominal');
//            } else {
//                $msd_awal_kredit = $request->input('nominal');
//                $msd_awal_debet = 0;
//            }
//
//            $data_master_detail = [
//                'master_id' => $master_id,
//                'msd_year' => date('Y'),
//                'msd_month' => date('j'),
//                'msd_awal_kredit' => $msd_awal_kredit,
//                'msd_awal_debet' => $msd_awal_debet
//            ];
//
//            mAcMasterDetail::create($data_master_detail);

            DB::commit();

            return General::response(General::$success, General::$store);
        } catch (\Exception $exception) {
            DB::rollback();

            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Detail COA
     *
     * @param Request $request
     * @return array
     */
    function detail(Request $request)
    {
        $rules = [
            'master_id' => [
                'required',
                'integer',
                new rMasterId()
            ]
        ];

        $attributes = [
            'master_id' => 'ID Master'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $master_id = $request->input('master_id');

        $data = mAcMaster::find($master_id);
        return General::response(General::$success, General::$get, $data);
    }

    /**
     * Update COA
     *
     * @param Request $request
     * @return array
     */
    function update(Request $request)
    {
        $rules = [
            'master_id' => [
                'required',
                'integer',
                new rMasterId()
            ],
            'mst_master_id' => 'required|integer',
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
            'mst_posisi' => 'required|in:neraca,laba rugi',
            'mst_normal' => 'required|in:debet,kredit',
            'mst_tipe_laporan' => 'required|in:laba kotor,laba bersih',
            'mst_tipe_nominal' => 'required|in:pendapatan,beban',
            'mst_neraca_tipe' => 'required|in:asset,liabilitas,ekuitas,null',
            'mst_kas_status' => 'required|in:yes,no',
            'mst_pembayaran' => 'required|in:yes,no',
        ];

        $attributes = [
            'master_id' => 'ID Master',
            'mst_master_id' => 'ID Master Parent',
            'mst_kode_rekening' => 'Kode Rekening',
            'mst_nama_rekening' => 'Nama Rekening',
            'mst_posisi' => 'Posisi',
            'mst_normal' => 'Normal',
            'mst_tipe_laporan' => 'Tipe Laporan',
            'mst_tipe_nominal' => 'Tipe Nominal',
            'mst_neraca_tipe' => 'Tipe Neraca',
            'mst_kas_status' => 'Status Kas',
            'mst_pembayaran' => 'Pembayaran',
        ];

        General::validator($request->all(), $rules, [], $attributes);

        DB::beginTransaction();
        try {

            $master_id = $request->input('master_id');
            $mst_master_id = $request->input('mst_master_id');
            $mst_kode_rekening = $request->input('mst_kode_rekening');
            $mst_nama_rekening = $request->input('mst_nama_rekening');
            $mst_posisi = $request->input('mst_posisi');
            $mst_normal = $request->input('mst_normal');
            $mst_tipe_laporan = $request->input('mst_tipe_laporan');
            $mst_tipe_nominal = $request->input('mst_tipe_nominal');
            $mst_neraca_tipe = $request->input('mst_neraca_tipe');
            $mst_kas_status = $request->input('mst_kas_status');
            $mst_pembayaran = $request->input('mst_pembayaran');

            $data_master = [
                'mst_master_id' => $mst_master_id,
                'mst_kode_rekening' => $mst_kode_rekening ,
                'mst_nama_rekening' => $mst_nama_rekening,
                'mst_posisi' => $mst_posisi,
                'mst_normal' => $mst_normal,
                'mst_tipe_laporan' => $mst_tipe_laporan,
                'mst_tipe_nominal' => $mst_tipe_nominal,
                'mst_neraca_tipe' => $mst_neraca_tipe,
                'mst_kas_status' => $mst_kas_status,
                'mst_pembayaran' => $mst_pembayaran,
            ];

            $master = mAcMaster::where('master_id', $master_id)->update($data_master);

//            /**
//             * Code dibawah ini adalah cara akunta pertama kali, setiap perubahan disimpan pada master detail
//             */
//            $master_id = $master->master_id;
//            if ($request->input('mst_normal') == 'debet') {
//                $msd_awal_kredit = 0;
//                $msd_awal_debet = $request->input('nominal');
//            } else {
//                $msd_awal_kredit = $request->input('nominal');
//                $msd_awal_debet = 0;
//            }
//
//            $data_master_detail = [
//                'master_id' => $master_id,
//                'msd_year' => date('Y'),
//                'msd_month' => date('j'),
//                'msd_awal_kredit' => $msd_awal_kredit,
//                'msd_awal_debet' => $msd_awal_debet
//            ];
//
//            mAcMasterDetail::create($data_master_detail);
            DB::commit();

            return General::response(General::$success, General::$update);
        } catch (\Exception $exception) {
            DB::rollback();

            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete COA
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'master_id' => [
                'required',
                'integer',
                new rMasterId()
            ]
        ];

        $attributes = [
            'master_id' => 'ID Master'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $master_id = $request->input('master_id');

        mAcMaster::where(['master_id' => $master_id])->delete();
        return General::response(General::$success, General::$delete);

    }

    function pembayaran()
    {
        $data = mAcMaster::where(['mst_pembayaran' => 'yes'])->get();
        return General::response(General::$success, General::$get, $data);
    }


}


