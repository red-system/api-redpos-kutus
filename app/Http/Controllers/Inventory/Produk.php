<?php

namespace App\Http\Controllers\Inventory;

use App\Helpers\General;
use App\Models\mProduk;
use App\Models\mProdukImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class Produk extends Controller
{

    public function __construct()
    {

    }

    /**
     * List Jenis Produk
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $data = mProduk::
            select('*')
            ->with([
                'jenis_nya:jenis_produk_id,jenis_produk_nama',
                'satuan_nya:satuan_id,satuan_nama',
            ])
            ->get();
        return General::response(General::$success, General::$get, $data);
    }

    /**
     *
     * Membuat Produk Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'barcode' => 'required',
            'produk_nama' => 'required',
            'produk_satuan_id' => 'required|integer',
            'produk_jenis_id' => 'required|integer',
            'produk_kode' => 'required',
            'harga_eceran' => 'required',
            'hpp_global' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',

        ];

        $attributes = [
            'barcode' => 'Barcode',
            'produk_nama' => 'Nama Produk',
            'produk_satuan_id' => 'Satuan Produk',
            'produk_jenis_id' => 'Jenis Produk',
            'produk_kode' => 'Kode Produk',
            'harga_eceran' => 'Harga Jual',
            'hpp_global' => 'Harga Pokok',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $barcode = $request->input('barcode');
            $produk_nama = $request->input('produk_nama');
            $produk_satuan_id = $request->input('produk_satuan_id');
            $produk_jenis_id = $request->input('produk_jenis_id');
            $produk_kode = $request->input('produk_kode');
            $harga_eceran = $request->input('harga_eceran');
            $harga_grosir = $request->input('harga_grosir')==null?0:$request->input('harga_grosir');
            $harga_konsinyasi = $request->input('harga_konsinyasi')==null?0:$request->input('harga_konsinyasi');
            $produk_minimal_stock = $request->input('produk_minimal_stock');
            $hpp_global = $request->input('hpp_global');
            $disc_persen = $request->input('disc_persen')==null?0:$request->input('disc_persen');
            $disc_nominal = $request->input('disc_nominal')==null?0:$request->input('disc_nominal');
            $meta_title = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description = $request->input('meta_description');

            $data = [
                'barcode' => $barcode,
                'produk_nama' => $produk_nama,
                'produk_satuan_id' => $produk_satuan_id,
                'produk_jenis_id' => $produk_jenis_id,
                'produk_kode' => $produk_kode,
                'harga_eceran' => $harga_eceran,
                'harga_grosir' => $harga_grosir,
                'harga_konsinyasi' => $harga_konsinyasi,
                'produk_minimal_stock' => $produk_minimal_stock,
                'hpp_global' => $hpp_global,
                'disc_persen' => $disc_persen,
                'disc_nominal' => $disc_nominal,
                'meta_title' => $meta_title,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
            ];

            mProduk::create($data);
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    /**
     * Update Produk
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'produk_id' => 'required|integer',
            'barcode' => 'required',
            'produk_nama' => 'required',
            'produk_satuan_id' => 'required|integer',
            'produk_jenis_id' => 'required|integer',
            'produk_kode' => 'required',
            'harga_eceran' => 'required',
            'hpp_global' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',

        ];

        $attributes = [
            'produk_id' => 'ID Produk',
            'barcode' => 'Barcode',
            'produk_nama' => 'Nama Produk',
            'produk_satuan_id' => 'Satuan Produk',
            'produk_jenis_id' => 'Jenis Produk',
            'produk_kode' => 'Kode Produk',
            'harga_eceran' => 'Harga Jual',
            'hpp_global' => 'Harga Pokok',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $produk_id = $request->input('produk_id');
            $barcode = $request->input('barcode');
            $produk_nama = $request->input('produk_nama');
            $produk_satuan_id = $request->input('produk_satuan_id');
            $produk_jenis_id = $request->input('produk_jenis_id');
            $produk_kode = $request->input('produk_kode');
            $harga_eceran = $request->input('harga_eceran');
            $harga_grosir = $request->input('harga_grosir')==null?0:$request->input('harga_grosir');
            $harga_konsinyasi = $request->input('harga_konsinyasi')==null?0:$request->input('harga_konsinyasi');
            $produk_minimal_stock = $request->input('produk_minimal_stock');
            $hpp_global = $request->input('hpp_global');
            $disc_persen = $request->input('disc_persen')==null?0:$request->input('disc_persen');
            $disc_nominal = $request->input('disc_nominal')==null?0:$request->input('disc_nominal');
            $meta_title = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description = $request->input('meta_description');

            $data = [
                'barcode' => $barcode,
                'produk_nama' => $produk_nama,
                'produk_satuan_id' => $produk_satuan_id,
                'produk_jenis_id' => $produk_jenis_id,
                'produk_kode' => $produk_kode,
                'harga_eceran' => $harga_eceran,
                'harga_grosir' => $harga_grosir,
                'harga_konsinyasi' => $harga_konsinyasi,
                'produk_minimal_stock' => $produk_minimal_stock,
                'hpp_global' => $hpp_global,
                'disc_persen' => $disc_persen,
                'disc_nominal' => $disc_nominal,
                'meta_title' => $meta_title,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
            ];

            mProduk::where('produk_id', $produk_id)->update($data);

            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Produk
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'produk_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'produk_id' => 'ID Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $produk_id = $request->input('produk_id');

        $produk_image = mProduk::select('*')
                                ->with('gambar_nya:*')
                                ->find($produk_id);

        foreach ($produk_image->gambar_nya as $item){
            $ada = Storage::disk('sftp')->exists($item->url);
            if ($ada) {
                Storage::disk('sftp')->delete($item->url);
            }
        }
        mProdukImage::where(['produk_id' => $produk_id])->delete();
        mProduk::where(['produk_id' => $produk_id])->delete();
        return General::response(General::$success, General::$delete);
    }

    /**
     * Detail Jenis Produk
     */
    function detail(Request $request)
    {
        $rules = [
            'produk_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'produk_id' => 'ID Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $produk_id = $request->input('produk_id');

        $data = mProduk::select('*')
            ->with([
                'jenis_nya:jenis_produk_id,jenis_produk_nama',
                'satuan_nya:satuan_id,satuan_nama',
                'gambar_nya:*',
                'jumlah_global_nya:*',
                'jumlah_per_lokasi:*',
                'jumlah_per_lokasi.lokasi_nya:*',
                'jumlah_per_lokasi.lokasi_nya.propinsi_nya:*',
                'jumlah_per_lokasi.lokasi_nya.kota_nya:*',
            ])
            ->find($produk_id);
        return General::response(General::$success, General::$get, $data);
    }
}


