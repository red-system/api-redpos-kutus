<?php

namespace App\Http\Controllers\Inventory;

use App\Helpers\General;
use App\Models\mArusStockProduk;
use App\Models\mCustomer;
use App\Models\mOrder;
use App\Models\mOrderDetail;
use App\Models\mPenjualan;
use App\Models\mPenjualanProduk;
use App\Models\mStockProduk;
use App\Rules\rOrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class Order extends Controller
{

    public function __construct()
    {

    }

    /**
     * List  Order
     *
     * @param Request $request
     * @return array
     */
    function index(Request $request)
    {

        $data = mOrder::
        select('*')
            ->with('detail_nya:*')
            ->paginate(10);
        return General::response(General::$success, General::$get, $data);
    }

    /**
     *
     * Membuat Order Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'customer_id' => 'required|integer',
            'lokasi_id' => 'required|integer',
            'tanggal' => 'required|date_format:Y-m-d',
            'total' => 'required',
            'additional' => 'required',
            'discount' => 'required',
            'grand_total' => 'required',
            'status' => 'required',
            'expired' => 'required|date_format:Y-m-d H:i:s',
            'item' => ['required',new rOrderDetail()],
        ];

        $attributes = [
            'lokasi_id' => 'ID Lokasi',
            'customer_id' => 'ID Customer',
            'tanggal' => 'Tanggal Order',
            'total' => 'Total Harga',
            'additional' => 'Biaya Tambahan',
            'discount' => 'Discount',
            'grand_total' => 'Grand Total',
            'status' => 'Status Order',
            'expired' => 'Waktu Expired',
            'item' => 'Item Order',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $customer_id = $request->input('customer_id');
            $lokasi_id = $request->input('lokasi_id');
            $tanggal = $request->input('tanggal');
            $total = $request->input('total');
            $additional = $request->input('additional');
            $discount = $request->input('discount');
            $grand_total = $request->input('grand_total');
            $status = $request->input('status');
            $expired = $request->input('expired');
            $item = $request->input('item');

            $data = [
                'customer_id' => $customer_id,
                'tanggal' => $tanggal,
                'total' => $total,
                'additional' => $additional,
                'discount' => $discount,
                'grand_total' => $grand_total,
                'status' => $status,
                'lokasi_id' => $lokasi_id,
                'expired' => $expired,
            ];

            $order = mOrder::create($data);
            $order_ecommerce_id = $order->order_ecommerce_id;
            $order_detail = array();
            foreach ($item as $value){
                $temp = array();

                $temp['order_ecommerce_id'] = $order_ecommerce_id;
                $temp['harga'] = $value['harga'];
                $temp['jumlah'] = $value['jumlah'];
                $temp['sub_total'] = $value['sub_total'];
                $temp['created_at'] = date("Y-m-d H:i:s");
                if(isset($value['produk_id'])){
                    $temp['produk_id'] = $value['produk_id'];
                }
                if(isset($value['produk_nama'])){
                    $temp['produk_nama'] = $value['produk_nama'];
                }
                if(isset($value['produk_satuan'])){
                    $temp['produk_satuan'] = $value['produk_satuan'];
                }
                array_push($order_detail,$temp);
            }
            mOrderDetail::insert($order_detail);
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    /**
     * Update Order
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'order_ecommerce_id' => 'required|integer',
            'lokasi_id' => 'required|integer',
            'customer_id' => 'required|integer',
            'tanggal' => 'required|date_format:Y-m-d',
            'total' => 'required',
            'additional' => 'required',
            'discount' => 'required',
            'grand_total' => 'required',
            'status' => 'required',
            'expired' => 'required|date_format:Y-m-d H:i:s',
            'item' => ['required',new rOrderDetail()],
        ];

        $attributes = [
            'order_ecommerce_id' => 'ID Order',
            'lokasi_id' => 'ID Lokasi',
            'customer_id' => 'ID Customer',
            'tanggal' => 'Tanggal Order',
            'total' => 'Total Harga',
            'additional' => 'Biaya Tambahan',
            'discount' => 'Discount',
            'grand_total' => 'Grand Total',
            'status' => 'Status Order',
            'expired' => 'Waktu Expired',
            'item' => 'Item Order',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        DB::beginTransaction();
        try {
            $customer_id = $request->input('customer_id');
            $lokasi_id = $request->input('lokasi_id');
            $tanggal = $request->input('tanggal');
            $total = $request->input('total');
            $additional = $request->input('additional');
            $discount = $request->input('discount');
            $grand_total = $request->input('grand_total');
            $status = $request->input('status');
            $expired = $request->input('expired');
            $item = $request->input('item');

            $data = [
                'customer_id' => $customer_id,
                'tanggal' => $tanggal,
                'total' => $total,
                'additional' => $additional,
                'lokasi_id' => $lokasi_id,
                'discount' => $discount,
                'grand_total' => $grand_total,
                'status' => $status,
                'expired' => $expired,
            ];


            $order_ecommerce_id = $request->input('order_ecommerce_id');
            mOrder::where('order_ecommerce_id', $order_ecommerce_id)->update($data);
            $this->kembalikan_stock($order_ecommerce_id);
            mOrderDetail::where(['order_ecommerce_id'=> $order_ecommerce_id])->delete();
            $order_detail = array();
            foreach ($item as $value){
                $temp = array();
                $temp['order_ecommerce_id'] = $order_ecommerce_id;
                $temp['harga'] = $value['harga'];
                $temp['jumlah'] = $value['jumlah'];
                $temp['sub_total'] = $value['sub_total'];
                $temp['created_at'] = date("Y-m-d H:i:s");
                if(isset($value['produk_id'])){
                    $temp['produk_id'] = $value['produk_id'];
                }
                if(isset($value['produk_nama'])){
                    $temp['produk_nama'] = $value['produk_nama'];
                }
                if(isset($value['produk_satuan'])){
                    $temp['produk_satuan'] = $value['produk_satuan'];
                }
                array_push($order_detail,$temp);
            }
            mOrderDetail::insert($order_detail);
            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Order
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'order_ecommerce_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'order_ecommerce_id' => 'ID Order'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $order_ecommerce_id = $request->input('order_ecommerce_id');
        $this->kembalikan_stock($order_ecommerce_id);
        mOrder::where(['order_ecommerce_id' => $order_ecommerce_id])->delete();
        mOrderDetail::where(['order_ecommerce_id' => $order_ecommerce_id])->delete();

        return General::response(General::$success, General::$delete);
    }

    /**
     * Detail  Order
     */
    function detail(Request $request)
    {
        $rules = [
            'order_ecommerce_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'order_ecommerce_id' => 'ID Order'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $order_ecommerce_id = $request->input('order_ecommerce_id');

        $data = mOrder::select('*')
            ->with('detail_nya:*')
            ->find($order_ecommerce_id);
        return General::response(General::$success, General::$get, $data);
    }
    function status(Request $request){
        $rules = [
            'order_ecommerce_id' => 'required|integer',
            'status'=>'required'
        ];

        $attributes = [
            'order_ecommerce_id' => 'ID Order',
            'status' => 'Status Order',

        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        DB::beginTransaction();
        try {
            $order_ecommerce_id = $request->input('order_ecommerce_id');
            $status = $request->input('status');
            $data = [
                'status' => $status,
            ];
            if($request->input('order_ecommerce_id')!=null){
                $data['tipe_pembayaran'] = $request->input('tipe_pembayaran');
            }
            mOrder::where('order_ecommerce_id', $order_ecommerce_id)->update($data);
            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    function potong_stock(Request $request){
        $rules = [
            'order_ecommerce_id' => 'required|integer',
        ];
        $attributes = [
            'order_ecommerce_id' => 'ID Order',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $mStock_produk = new mStockProduk();
        DB::beginTransaction();
        try {
            $order_ecommerce_id = $request->input('order_ecommerce_id');
            $order = mOrder::select('*')
                            ->with('detail_nya:*')
                            ->find($order_ecommerce_id);
            $lokasi_id = $order->lokasi_id;
            $tanggal = date("Y-m-d");
            foreach ($order->detail_nya as $item){
                $stock_produk = mStockProduk::where([
                                    'stock_produk_lokasi_id'=>$lokasi_id,
                                    'produk_id'=>$item->produk_id,
                                ])
                                ->get();
                $jumlah_item = $item->jumlah;
                $stock_produk_id_arr = array();
                $qty_stock = array();
                foreach ($stock_produk as $row) {
                    $data = array();
                    $jumlah = $row->stock_produk_qty - $jumlah_item;
                    $stock_out = $jumlah_item;
                    if($row->stock_produk_qty < $jumlah_item){
                        $jumlah = 0;
                        $stock_out = $row->stock_produk_qty;
                    }
                    $jumlah_item =  ($jumlah_item-$row->stock_produk_qty);
                    $data["stock_produk_qty"] = $jumlah;
                    $stock_produk_id = $row->stock_produk_id;
                    $updateStok = mStockProduk::where('stock_produk_id',$stock_produk_id)->update($data);
                    array_push($stock_produk_id_arr,$stock_produk_id);
                    array_push($qty_stock,$stock_out);
                    if($updateStok){
                        $data = array();
                        $data["order_ecommerce_id"] = $order_ecommerce_id;
                        $data["tanggal"] = $tanggal;
                        $data["table_name"] = "stock_produk";
                        $data["stock_produk_id"] = $row->stock_produk_id;
                        $data["produk_id"] = $item->produk_id;
                        $data["stock_out"] = $stock_out;
                        $data["stock_in"] = 0;
                        $data["last_stock"] = $mStock_produk->last_stock($row->produk_id)->result;
                        $data["last_stock_total"] = $mStock_produk->stock_total()->result;
                        $data["keterangan"] = "Order Produk no order ".$order->order_no;
                        $data["method"] = "update";
                        mArusStockProduk::create($data);
                    }
                    if($jumlah_item <= 0){
                        break;
                    }
                }
                $data = array();
                $data['stock_produk_id'] = implode('|',$stock_produk_id_arr);
                $data['stock_qty'] = implode('|',$qty_stock);
                mOrderDetail::where('order_ecommerce_detail_id',$item->order_ecommerce_detail_id)->update($data);

            }
            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    function kembalikan(Request $request){
        $rules = [
            'order_ecommerce_id' => 'required|integer',
        ];
        $attributes = [
            'order_ecommerce_id' => 'ID Order',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);
        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        $mStock_produk = new mStockProduk();
        DB::beginTransaction();
        try {
            $order_ecommrce_id = $request->input('order_ecommerce_id');
            $this->kembalikan_stock($order_ecommrce_id);
            DB::commit();
            return General::response(General::$success, General::$update);
        }catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
    function kembalikan_stock($order_ecommerce_id){
        $order = $order = mOrder::select('*')
            ->with('detail_nya:*')
            ->find($order_ecommerce_id);
        foreach ($order->detail_nya as $value){
            $data = array();
            $stock_produk_id_ar = $value->stock_produk_id != null ? explode("|",$value->stock_produk_id):array();
            $qty_stock = $value->stock_qty != null ? explode("|",$value->stock_qty):array();
            foreach ($stock_produk_id_ar as $key=>$item){
                $stock = mStockProduk::find($item);
                if($stock!=null){
                    $data['stock_produk_qty'] = $stock->stock_produk_qty+$qty_stock[$key];
                    mStockProduk::where('stock_produk_id',$item)->update($data);

                }
            }
        }
        mArusStockProduk::where(['order_ecommerce_id'=>$order_ecommerce_id])->delete();

    }
    function salin(Request $request){
        $rules = [
            'order_ecommerce_id' => 'required|integer',
        ];

        $attributes = [
            'order_ecommerce_id' => 'ID Order',
        ];
        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }
        DB::beginTransaction();
        try {
            $order_ecommerce_id = $request->input('order_ecommerce_id');
            $data = array();
            if($request->input('status')!=null){
                $data['status'] = $request->input('status');
            }
            if($request->input('tipe_pembayaran')!=null){
                $data['tipe_pembayaran'] = $request->input('tipe_pembayaran');
            }
            mOrder::where('order_ecommerce_id', $order_ecommerce_id)->update($data);
            $order = mOrder::select('*')
                            ->with('detail_nya:*')
                            ->find($order_ecommerce_id);
            $customer = mCustomer::find($order->customer_id);

            $penjualan = array();
            $penjualan['lokasi_id'] = $order->lokasi_id;
            $penjualan['tipe_pembayaran_nama'] = $request->input('tipe_pembayaran');
            $penjualan['nama_pelanggan'] = $customer->name;
            $penjualan['alamat_pelanggan'] = $customer->alamat;
            $penjualan['telepon_pelanggan'] = $customer->telephone;
            $penjualan['status_pembayaran'] = 'Lunas';
            $penjualan['tanggal'] = date('Y-m-d');
            $penjualan['jenis_transaksi'] = 'distributor';
            $penjualan['total'] = $order->total;
            $penjualan['biaya_tambahan'] = $order->additional;
            $penjualan['potongan_akhir'] = $order->discount;
            $penjualan['grand_total'] = $order->grand_total;
            $penjualan['terbayar'] = $order->grand_total;
            $penjualan['sisa_pembayaran'] = 0;
            $penjualan['kembalian'] = 0;
            $penjualan['order_ecommerce_id'] = $order->order_ecommerce_id;
            $insertPenjualan = mPenjualan::create($penjualan);

            foreach ($order->detail_nya as $item){
                $penjualanproduk = array();
                $penjualanproduk['penjualan_id'] = $insertPenjualan->penjualan_id;
                $penjualanproduk['produk_id'] = $item->produk_id;
                $penjualanproduk['satuan_nama'] = $item->produk_satuan;
                $penjualanproduk['qty'] = $item->jumlah;
                $penjualanproduk['harga'] = $item->harga;
                $penjualanproduk['potongan'] = $item->discount;
                $penjualanproduk['harga_net'] = $item->harga;
                $penjualanproduk['stock_produk_id'] = $item->stock_produk_id;
                $penjualanproduk['qty_stock'] = $item->stock_qty;
                $penjualanproduk['sub_total'] = $item->sub_total;
                $insertPenjualanProduk = mPenjualanProduk::create($penjualanproduk);

            }
            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }
}


