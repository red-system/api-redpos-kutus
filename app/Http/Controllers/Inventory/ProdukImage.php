<?php

namespace App\Http\Controllers\Inventory;

use App\Helpers\General;
use App\Helpers\Main;
use App\Models\mProduk;
use App\Models\mProdukImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class ProdukImage extends Controller
{

    public function __construct()
    {

    }

    /**
     *
     * Membuat Produk Image Baru
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function store(Request $request)
    {
        $rules = [
            'produk_id' => 'required|integer',
            'title' => 'required',
            'alt' => 'required',
            'type' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',
            'produk_image' => 'required|image',

        ];
        $attributes = [
            'produk_id' => 'ID Produk',
            'title' => 'Judul Gambar',
            'alt' => 'Alt Gambar',
            'type' => 'Type Gambar',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'produk_image' => 'Gambar Produk',
        ];

        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $produk_id = $request->input('produk_id');
            $title = $request->input('title');
            $alt = $request->input('alt');
            $type = $request->input('type');
            $meta_title = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description= $request->input('meta_description');

            $data = [
                'produk_id' => $produk_id,
                'title' => $title,
                'alt' => $alt,
                'type' => $type,
                'meta_title' => $meta_title,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
            ];
            $var = new Main();
            $image_dir = $var->image_dir();
            $main = new Main();
            $filename = $main->slug($title).'-'.time();
            $saveUrl = '';
            if ($request->hasFile('produk_image')) {
                $path = $image_dir;
                $file = $request->file('produk_image');
                $cek = Storage::disk('sftp')->putFileAs($path, $file, $filename. $file->getClientOriginalExtension());
                $saveUrl = $cek;
            }
            $data['url'] = $saveUrl;
            mProdukImage::create($data);
            DB::commit();
            return General::response(General::$success, General::$store);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }

    }
    /**
     * Update Produk Image
     *
     * @param Request $request
     * @return array|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    function update(Request $request)
    {
        $rules = [
            'produk_image_id' => 'required|integer',
            'produk_id' => 'required|integer',
            'title' => 'required',
            'alt' => 'required',
            'type' => 'required',
            'meta_title' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',

        ];
        $attributes = [
            'produk_image_id' => 'ID Gambar Produk',
            'produk_id' => 'ID Produk',
            'title' => 'Judul Gambar',
            'alt' => 'Alt Gambar',
            'type' => 'Type Gambar',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
        ];


        $attributes = array_merge($attributes, $attributes);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        DB::beginTransaction();
        try {
            $produk_image_id = $request->input('produk_image_id');
            $produk_id = $request->input('produk_id');
            $title = $request->input('title');
            $alt = $request->input('alt');
            $type = $request->input('type');
            $meta_title = $request->input('meta_title');
            $meta_keyword = $request->input('meta_keyword');
            $meta_description= $request->input('meta_description');

            $data = [
                'produk_id' => $produk_id,
                'title' => $title,
                'alt' => $alt,
                'type' => $type,
                'meta_title' => $meta_title,
                'meta_keyword' => $meta_keyword,
                'meta_description' => $meta_description,
            ];
            if ($request->hasFile('produk_image')) {
                $var = new Main();
                $image_dir = $var->image_dir();
                $main = new Main();
                $filename = $main->slug($title).'-'.time();
                $path = $image_dir;
                $file = $request->file('produk_image');
                $oldData = mProdukImage::find($produk_image_id);

                $ada = Storage::disk('sftp')->exists($oldData->url);

                if ($ada) {
                    Storage::disk('sftp')->delete($oldData->url);
                }
                $url = Storage::disk('sftp')->putFileAs($path, $file, $filename. $file->getClientOriginalExtension());
                $data['url'] = $url;
            }

            mProdukImage::where('produk_image_id', $produk_image_id)->update($data);

            DB::commit();
            return General::response(General::$success, General::$update);

        } catch (\Exception $exception) {
            throw $exception;
            DB::rollback();
            return General::response(General::$error, General::$validation_error);
        }
    }

    /**
     * Delete Produk Image
     *
     * @param Request $request
     * @return array
     */
    function delete(Request $request)
    {
        $rules = [
            'produk_image_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'produk_image_id' => 'ID Gambar Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);
        $produk_image_id = $request->input('produk_image_id');
        $oldData = mProdukImage::find($produk_image_id);
        $ada = Storage::disk('sftp')->exists($oldData->url);
        if ($ada) {
            Storage::disk('sftp')->delete($oldData->url);
        }
        mProdukImage::where(['produk_image_id' => $produk_image_id])->delete();
        return General::response(General::$success, General::$delete);

    }

    /**
     * Detail Jenis Produk
     */
    function detail(Request $request)
    {
        $rules = [
            'produk_image_id' => [
                'required',
                'integer',
            ]
        ];

        $attributes = [
            'produk_image_id' => 'ID Gambar Produk'
        ];

        General::validator($request->all(), $rules, [], $attributes);

        $produk_image_id = $request->input('produk_image_id');

        $data = mProdukImage::find($produk_image_id);
        return General::response(General::$success, General::$get, $data);
    }

}


