<?php

namespace app\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use app\Models\mBahan;
use app\Models\mProduk;
use app\Models\mLokasi;
use app\Models\mKategoriProduk;
use app\Models\mStokProduk;
use App\Helpers\General;


class Main
{

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 0, '', '.');
        }
    }
    function slug($text)
    {

        $find = array(' ','’',':', '/', '&', '\\', '\'', ',','(',')');
        $replace = array('-','-','-', '-', 'and', '-', '-', '-','','');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }
    function image_dir(){
        return 'redpos/'.General::company_detail()['database_name'];
    }
    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, ',', '.');
        } else {
            return number_format($number, 0, '', '.');
        }
    }

    public static function format_number_system($number) {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number) {
        $number = str_replace(['.',','], ['','.'], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if( $number - floor($number) >= 0.1  ) {
            return true;
        } else {
            return false;
        }
    }

    public static function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
