<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcAsset;

class mAcAssetPenyusutan extends Model
{

    protected $table = 'tb_ac_asset_penyusutan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_asset',
        'penyusutan_perbulan',
        'bulan',
        'tahun',
    ];

    function asset()
    {
        return $this->belongsTo(mAcAsset::class, 'id_asset');
    }
}
