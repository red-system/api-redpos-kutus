<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;
use App\Models\mProduk;

class mJenisProduk extends Model
{
    protected $table = 'jenis_produk';
    protected $primaryKey = 'jenis_produk_id';
    protected $fillable = [
        'jenis_produk_id',
        'jenis_produk_kode',
        'jenis_produk_nama',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
    ];
    public static function create(array $data = [])
    {
        $data['created_at'] = date("Y-m-d H:i:s");

        $model = static::query()->create($data);
        return $model;
    }
    function produk_nya()
    {
        return $this->hasMany(mProduk::class, 'produk_jenis_id','jenis_produk_id');
    }


}
