<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\mPenjualanProduk;


class mPenjualan extends Model
{
    protected $table = 'penjualan';
    protected $primaryKey = 'penjualan_id';
    protected $fillable = [
        'ekspedisi_id',
        'order_ecommerce_id',
        'lokasi_id',
        'tipe_pembayaran_id',
        'log_kasir_id',
        'nama_pelanggan',
        'alamat_pelanggan',
        'telepon_pelanggan',
        'urutan',
        'no_faktur',
        'status_pembayaran',
        'status_distribusi',
        'tanggal',
        'jenis_transaksi',
        'pengiriman',
        'total',
        'biaya_tambahan',
        'potongan_akhir',
        'grand_total',
        'terbayar',
        'sisa_pembayaran',
        'kembalian',
        'order_name',
        'order_note',
        'status',
        'additional_no',
        'additional_tanggal',
        'additional_keterangan',
        'status_distribusi_done_at',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',

    ];
    public static function create(array $data = [])
    {
        $prefix = "PL-".date("ymd")."/".$data['lokasi_id'].'/';
        $cek = DB::table('penjualan')
            ->where('no_faktur', 'like', '%'.$prefix.'%')
            ->orderBy('no_faktur','desc')
            ->first();
        $order_no = $cek == null ? $prefix."1":$prefix.((explode('/'.$data['lokasi_id'].'/',$cek->no_faktur)[1])+1);
        $urutan = $cek == null ? 1 :((explode('/'.$data['lokasi_id'].'/',$cek->no_faktur)[1])+1);
        $data['no_faktur'] = $order_no;
        $data['urutan'] = $urutan;
        $model = static::query()->create($data);
        return $model;
    }
    function produk_nya(){
        return $this->hasMany(mPenjualanProduk::class,'penjualan_id','penjualan_id');
    }

}
