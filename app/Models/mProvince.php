<?php

namespace app\Models;

use App\Helpers\General;
use App\Models\mCity;
use Illuminate\Database\Eloquent\Model;


class mProvince extends Model
{
    protected $table = 'province';
    protected $primaryKey = 'province_id';
    protected $fillable = [
        'province'
    ];
    function kota_nya(){
        return $this->hasMany(mCity::class,'province_id','province_id');
    }

}
