<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mAcMaster extends Model
{
    protected $table = 'tb_ac_master';
    protected $primaryKey = 'master_id';
    protected $fillable = [
        'master_id',
        'mst_master_id',
        'mst_kode_rekening',
        'master_id',
        'mst_master_id',
        'mst_kode_rekening',
        'mst_nama_rekening',
        'mst_tanggal_awal',
        'mst_posisi',
        'mst_normal',
        'mst_status',
        'mst_tipe_laporan',
        'mst_tipe_nominal',
        'mst_neraca_tipe',
        'mst_kas_status',
        'mst_pembayaran'
    ];

    public function transaksi()
    {
        return $this->hasMany(mAcTransaksi::class, 'master_id');
    }

    public function master_detail()
    {
        return $this->hasMany(mAcMasterDetail::class,  'master_id');
    }

    public function childs()
    {
        return $this->hasMany(mAcMaster::class, 'mst_master_id');
    }

    public function parent()
    {
        return $this->belongsTo(mAcMasterDetail::class, 'mst_master_id');
    }


    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
