<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mProdukStockGlobal extends Model
{
    protected $table = 'display_stock_produk';
    protected $primaryKey = 'produk_id';
    protected $fillable = [
        'jumlah',
    ];
}
