<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mSatuan extends Model
{
    protected $table = 'satuan';
    protected $primaryKey = 'satuan_id';
    protected $fillable = [
        'satuan_id',
        'satuan_kode',
        'satuan_nama',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
    ];
    public static function create(array $data = [])
    {
        $data['created_at'] = date("Y-m-d H:i:s");

        $model = static::query()->create($data);
        return $model;
    }
}
