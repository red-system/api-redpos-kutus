<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobReseller extends Model
{
    protected $table = 'reseller';
    protected $primaryKey = 'reseller_id';

    public function provincie(){
        return $this->belongsTo(mobProvincie::class, 'provincie_id');
    }
    public function city(){
        return $this->belongsTo(mobCity::class, 'city_id');
    }
    public function district(){
        return $this->belongsTo(mobDistrict::class, 'subdistrict_id');
    }
}
