<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobProvincie extends Model
{
    protected $table = 'province';
    protected $primaryKey = 'province_id';
    protected $fillable = [
        'province',
    ];

}
