<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobProduk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'produk_id';
    protected $fillable = [
        'barcode',
        'produk_nama',
        'produk_satuan_id',
        'produk_jenis_id',
        'produk_kode',
        'harga_eceran',
        'harga_grosir',
        'harga_konsinyasi',
        'produk_minimal_stock',
        'hpp_global',
        'disc_persen',
        'disc_nominal',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'created_at',
        'updated_at'
    ];

    public function satuan_produk(){
        return $this->belongsTo(mobProdukSatuan::class, 'produk_satuan_id');
    }

    public function jenis_produk(){
        return $this->belongsTo(mobProdukJenis::class, 'produk_jenis_id');
    }

}
