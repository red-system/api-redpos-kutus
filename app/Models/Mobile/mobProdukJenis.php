<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobProdukJenis extends Model
{
    protected $table = 'jenis_produk';
    protected $primaryKey = 'jenis_produk_id';
    protected $fillable = [
        'jenis_produk_kode',
        'jenis_produk_nama',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'created_at',
        'updated_at'
    ];

}
