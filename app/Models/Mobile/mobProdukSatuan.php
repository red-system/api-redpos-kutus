<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobProdukSatuan extends Model
{
    protected $table = 'satuan';
    protected $primaryKey = 'satuan_id';
    protected $fillable = [
        'satuan_kode',
        'satuan_nama',
        'created_at',
        'updated_at'
    ];

}
