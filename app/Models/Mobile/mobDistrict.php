<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobDistrict extends Model
{
    protected $table = 'subdistrict';
    protected $primaryKey = 'subdistrict_id';
    protected $fillable = [
        'city_id',
        'subdistrict_name'
    ];

}
