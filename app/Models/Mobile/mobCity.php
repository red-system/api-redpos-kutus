<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobCity extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'city_id';
    protected $fillable = [
        'province_id',
        'type',
        'city',
        'postal_code'
    ];

}
