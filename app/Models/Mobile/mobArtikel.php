<?php

namespace App\Models\Mobile;

use Illuminate\Database\Eloquent\Model;

class mobArtikel extends Model
{
    protected $table = 'artikel';
    protected $primaryKey = 'artikel_id';
    protected $fillable = [
        'title',
        'description',
        'image',
        'active',
        'created_at',
        'updated_at'
    ];

}
