<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mProdukImage extends Model
{
    protected $table = 'produk_image';
    protected $primaryKey = 'produk_image_id';
    protected $fillable = [
        'produk_id',
        'title',
        'alt',
        'url',
        'type',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',

    ];
    protected $appends = ['image_url'];
    public function getImageUrlAttribute() {
        return url(env('IMAGE_STORAGE_URL').$this->url);
    }
    public static function create(array $data = [])
    {
        $data['created_at'] = date("Y-m-d H:i:s");

        $model = static::query()->create($data);
        return $model;
    }
}
