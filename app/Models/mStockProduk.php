<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class mStockProduk extends Model
{
    protected $table = 'stock_produk';
    protected $primaryKey = 'stock_produk_id';
    protected $fillable = [
        'produk_id',
        'stock_produk_lokasi_id',
        'month',
        'year',
        'urutan',
        'stock_produk_seri',
        'stock_produk_qty',
        'hpp',
        'last_stok',
        'stock_produk_keterangan',
        'po_id',
        'delete_flag',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
    ];
    function last_stock($produk_id){
        return DB::table('stock_produk')
            ->select(DB::raw('sum(stock_produk.stock_produk_qty) as result'))
            ->where([
                'produk_id'=>$produk_id,
                'delete_flag'=>0
            ])
            ->first();
    }
    function stock_total(){
        return DB::table('stock_produk')
            ->select(DB::raw('sum(stock_produk.stock_produk_qty) as result'))
            ->first();
    }

}
