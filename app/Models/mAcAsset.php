<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcAssetPenyusutan;
use App\Models\mAcAssetKategori;
use App\Models\mAcMaster;

class mAcAsset extends Model
{

    protected $table = 'tb_ac_asset';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_asset_kategori',
        'kode_asset',
        'nama',
        'tanggal_beli',
        'tanggal_beli_2',
        'qty',
        'harga_beli',
        'provisi',
        'total_beli',
        'nilai_residu',
        'umur_ekonomis',
        'lokasi',
        'akumulasi_beban',
        'terhitung_tanggal',
        'nilai_buku',
        'beban_perbulan',
        'metode',
        'tanggal_pensiun',
        'master_id_asset',
        'master_id_akumulasi',
        'master_id_depresiasi',
        'master_id_jenis_pembayaran'
    ];

    function asset_kategori() {
        return $this->belongsTo(mAcAssetKategori::class, 'id_asset_kategori');
    }

    public function master_asset(){
        return $this->belongsTo(mAcMaster::class,'master_id_asset');
    }

    public function master_akumulasi(){
        return $this->belongsTo(mAcMaster::class,'master_id_akumulasi');
    }

    public function master_depresiasi(){
        return $this->belongsTo(mAcMaster::class,'master_id_depresiasi');
    }

    public function master_jenis_pembayaran(){
        return $this->belongsTo(mAcMaster::class,'master_id_jenis_pembayaran');
    }

    public function penyusutan_asset(){
        return $this->hasMany(mAcAssetPenyusutan::class,'id_asset');
    }

}
