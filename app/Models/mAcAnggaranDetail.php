<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcAnggaran;
use App\Models\mAcMaster;

class mAcAnggaranDetail extends Model
{
    protected $table = 'tb_ac_anggaran_detail';
    protected $primaryKey = 'anggaran_detail_id';
    protected $fillable = [
        'anggaran_id',
        'master_id',
        'anggaran_nominal',
    ];

    function anggaran()
    {
        return $this->belongsTo( mAcAnggaran::class, 'anggaran_id');
    }

    function master() {
        return $this->belongsTo(mAcMaster::class, 'master_id');
    }
}
