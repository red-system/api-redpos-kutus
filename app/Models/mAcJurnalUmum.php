<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcTransaksi;

class mAcJurnalUmum extends Model
{
    // public $incrementing = false;
    protected $table = 'tb_ac_jurnal_umum';
    protected $primaryKey = 'jurnal_umum_id';
    protected $fillable = [
        'user_id',
        'user_name',
        'user_table_name',
        'no_invoice',
        'jmu_field_value',
        'jmu_field_name',
        'jmu_table_name',
        'jmu_tanggal',
        'jmu_no',
        'jmu_keterangan',
        'jmu_year',
        'jmu_month',
        'jmu_day',
        'edit',
        'reference_number'
    ];

    function transaksi()
    {
        return $this->hasMany( mAcTransaksi::class, 'jurnal_umum_id');
    }
}
