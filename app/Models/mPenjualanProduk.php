<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class mPenjualanProduk extends Model
{
    protected $table = 'penjualan_produk';
    protected $primaryKey = 'penjualan_produk_id';
    protected $fillable = [
        'penjualan_id',
        'produk_id',
        'satuan_id',
        'nilai_konversi',
        'satuan_nama',
        'produk_custom_id',
        'qty',
        'harga',
        'hpp',
        'potongan',
        'ppn_persen',
        'ppn_nominal',
        'harga_net',
        'stock_produk_id',
        'qty_stock',
        'sub_total',
        'keterangan',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',

    ];
}
