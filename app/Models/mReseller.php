<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mReseller extends Model
{
    protected $table = 'reseller';
    protected $primaryKey = 'reseller_id';
    protected $fillable = [
        'reseller'
    ];
    protected $appends = ['referal_url'];

    public function getReferalUrlAttribute() {
        return "http://app.support88.id/register-v1/".$this->referal_code;
    }


}
