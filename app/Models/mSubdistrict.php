<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mSubdistrict extends Model
{
    protected $table = 'subdistrict';
    protected $primaryKey = 'subdistrict_id';
    protected $fillable = [
        'subdistrict'
    ];


}
