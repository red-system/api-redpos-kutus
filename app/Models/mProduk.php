<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;
use App\Models\mJenisProduk;
use App\Models\mSatuan;
use App\Models\mProdukImage;
use App\Models\mProdukStockGlobal;
use App\Models\mProdukStockLokasi;

class mProduk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'produk_id';
    protected $fillable = [
        'barcode',
        'produk_nama',
        'produk_satuan_id',
        'produk_jenis_id',
        'produk_kode',
        'harga_eceran',
        'harga_grosir',
        'harga_konsinyasi',
        'produk_minimal_stock',
        'hpp_global',
        'disc_persen',
        'disc_nominal',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
        'meta_tilte',
        'meta_keyword',
        'meta_description',
    ];
    public static function create(array $data = [])
    {
        $data['created_at'] = date("Y-m-d H:i:s");

        $model = static::query()->create($data);
        return $model;
    }
    function jenis_nya(){
        return $this->belongsTo(mJenisProduk::class,'produk_jenis_id','jenis_produk_id');
    }
    function satuan_nya(){
        return $this->belongsTo(mSatuan::class,'produk_satuan_id','satuan_id');
    }
    function gambar_nya(){
        return $this->hasMany(mProdukImage::class,'produk_id','produk_id');
    }
    function jumlah_global_nya(){
        return $this->belongsTo(mProdukStockGlobal::class,'produk_id','produk_id');
    }
    function jumlah_per_lokasi(){
        return $this->hasMany(mProdukStockLokasi::class,'produk_id','produk_id');
    }
}
