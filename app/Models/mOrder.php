<?php

namespace app\Models;

use App\Helpers\General;
use App\Models\mOrderDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mOrder extends Model
{
    protected $table = 'order_ecommerce';
    protected $primaryKey = 'order_ecommerce_id';
    protected $fillable = [
        'order_ecommerce_id',
        'tipe_pembayaran',
        'customer_id',
        'lokasi_id',
        'order_no',
        'tanggal',
        'total',
        'additional',
        'discount',
        'grand_total',
        'status',
        'expired',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
    ];
    public static function create(array $data = [])
    {
        $prefix = "OE-".date("ymd")."/";
        $cek = DB::table('order_ecommerce')
            ->where('order_no', 'like', '%'.$prefix.'%')
            ->orderBy('order_no','desc')
            ->first();
        $order_no = $cek == null ? $prefix."1":$prefix.((explode('/',$cek->order_no)[1])+1);
        $data['order_no'] = $order_no;
        $model = static::query()->create($data);
        return $model;
    }
    function detail_nya(){

        return $this->hasMany(mOrderDetail::class,'order_ecommerce_id','order_ecommerce_id');
    }


}
