<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcAnggaranDetail;
use App\Models\mAcMaster;
use App\Models\mAcTransaksi;

class mAcAnggaran extends Model
{
    protected $table = 'tb_ac_anggaran';
    protected $table_detail = 'tb_ac_anggaran_detail';
    protected $primaryKey = 'anggaran_id';
    protected $fillable = [
        'anggaran_tahun',
        'user_table_name',
        'user_id_created',
        'user_name_created',
        'user_id_updated',
        'user_name_updated',
    ];

    function anggaran_detail()
    {
        return $this->hasMany(mAcAnggaranDetail::class, 'anggaran_id');
    }

    function master() {
        return $this->hasManyThrough(mAcMaster::class, mAcAnggaranDetail::class);
    }
}
