<?php

namespace app\Models;

use App\Helpers\General;
use App\Models\mSubdistrict;
use Illuminate\Database\Eloquent\Model;


class mCity extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'city_id';
    protected $fillable = [
        "province_id",
        "type",
        "city_name",
        "postal_code",
    ];
    function kecamatannya(){
        return $this->hasMany(mSubdistrict::class,'city_id','city_id');
    }
}
