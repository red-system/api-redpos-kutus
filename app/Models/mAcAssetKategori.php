<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\mAcAsset;

class mAcAssetKategori extends Model
{

    protected $table = 'tb_ac_asset_kategori';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'penyusutan_status',
        'description',
    ];


    public function asset(){
        return $this->hasMany(mAcAsset::class,'id_asset_kategori');
    }
}
