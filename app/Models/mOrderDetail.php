<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mOrderDetail extends Model
{
    protected $table = 'order_ecommerce_detail';
    protected $primaryKey = 'order_ecommerce_detail_id';
    protected $fillable = [
        'order_ecommerce_id',
        'produk_id',
        'produk_nama',
        'produk_satuan',
        'harga',
        'jumlah',
        'sub_total',
        'additional',
        'discount',
        'grand_total',
        'stock_produk_id',
        'stock_qty',
        'created_at',
        'updated_at',
    ];


}
