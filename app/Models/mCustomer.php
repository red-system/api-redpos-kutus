<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mCustomer extends Model
{
    protected $table = 'customer';
    protected $primaryKey = 'customer_id';
    protected $fillable = [
        'name',
        'email',
        'kecamatan_id',
        'kabupaten_id',
        'provinsi_id',
        'alamat',
        'kode_pos',
        'telephone',
        'password',
        'created_at',
        'updated_at',
    ];
    public static function create(array $data = [])
    {
        $data['created_at'] = date("Y-m-d H:i:s");

        $model = static::query()->create($data);
        return $model;
    }
}
