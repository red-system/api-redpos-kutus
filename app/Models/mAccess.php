<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mAccess extends Model
{
    protected $table = 'access';
    protected $primaryKey = 'id';
    protected $fillable = [
        'token',
        'created_user_id',
        'created_at',
        'updated_user_id',
        'updated_at',
    ];
    public static function create(array $data = [])
    {
        $staff = General::user_detail();
        $staff_id = $staff->staff_id;

        $data['created_staff_id'] = $staff_id;

        $model = static::query()->create($data);
        return $model;
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
