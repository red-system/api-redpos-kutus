<?php

namespace app\Models;

use App\Helpers\General;
use App\Models\mLokasi;
use Illuminate\Database\Eloquent\Model;

class mProdukStockLokasi extends Model
{
    protected $table = 'display_stock_produk_lokasi';
    protected $fillable = [
        'produk_id',
        'jumlah',
    ];
    function lokasi_nya(){
        return $this->belongsTo(mLokasi::class,'stock_produk_lokasi_id','lokasi_id');
    }
}
