<?php

namespace app\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;


class mArusStockProduk extends Model
{
    protected $table = 'arus_stock_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'tanggal',
        'table_name',
        'order_ecommerce_id',
        'penjualan_id',
        'penjualan_produk_id',
        'distribusi_id',
        'distribusi_detail_id',
        'produksi_id',
        'progress_produksi_id',
        'stock_produk_id',
        'produk_id',
        'history_penyesuaian_produk_id',
        'stock_in',
        'stock_out',
        'last_stock',
        'last_stock_total',
        'method',
        'keterangan',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',
    ];

}
