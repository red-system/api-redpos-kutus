<?php

namespace app\Models;

use App\Helpers\General;
use App\Models\mProvince;
use App\Models\mCity;
use Illuminate\Database\Eloquent\Model;

class mLokasi extends Model
{
    protected $table = 'lokasi';
    protected $primaryKey = 'lokasi_id';
    protected $fillable = [
        'lokasi_kode',
        'lokasi_nama',
        'lokasi_tipe',
        'lokasi_alamat',
        'lokasi_telepon',
        'staff_created',
        'staff_updated',
        'created_at',
        'updated_at',

    ];
    function propinsi_nya(){
        return $this->belongsTo(mProvince::class,'province_id','province_id');
    }
    function kota_nya(){
        return $this->belongsTo(mCity::class,'city_id','city_id');
    }

}
