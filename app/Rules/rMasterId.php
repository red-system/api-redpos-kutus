<?php
namespace App\Rules;

use App\Models\mAcMaster;
use Illuminate\Contracts\Validation\ImplicitRule;

class rMasterId implements ImplicitRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $master_id = $value;
        $check = mAcMaster::where('master_id', $master_id)->count();
        if($check > 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ID Master tidak tersedia';
    }
}