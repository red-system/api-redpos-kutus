<?php
namespace App\Rules;

use App\Models\mAcAnggaran;
use App\Models\mAcMaster;
use Illuminate\Contracts\Validation\ImplicitRule;

class rAnggaranIdExist implements ImplicitRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $anggaran_id = $value;
        $check = mAcAnggaran::where('anggaran_id', $anggaran_id)->count();
        if($check > 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ID Anggaran tidak tersedia';
    }
}