<?php
namespace App\Rules;

use App\Models\mCustomer;
use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;
use Illuminate\Support\Facades\Hash;

class rLogin implements ImplicitRule
{
    protected $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email = $this->email;
        $password = $value;
        $password_db = mCustomer::where('email', $email)->value('password');
        $status = FALSE;

        /**
         * Login user
         */
        if (Hash::check($password, $password_db)) {
            $status = TRUE;
        }


        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email atau Password salah';
    }
}