<?php
namespace App\Rules;

use App\Models\mCustomer;
use Illuminate\Contracts\Validation\ImplicitRule;


class rCustomerStore implements ImplicitRule
{

    protected $email;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $this->email = $value;
        $check = mCustomer::where('email', $value)->count();
        if($check == 0) {
            $status = TRUE;
        }
        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email '.$this->email.' tidak tersedia';
    }
}