<?php

namespace App\Rules;

use App\Models\mAcMaster;
use App\Helpers\Main;
use Illuminate\Contracts\Validation\ImplicitRule;

class rOrderDetail implements ImplicitRule
{
    protected $message;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $oderDetail = $value;
        $cek = FALSE;
        foreach ($oderDetail as $item){
            if(!isset($item['harga'])){
                $this->message = 'Pastikan semua item memiliki harga';
                $cek = TRUE;
                break;

            }
            if(!isset($item['jumlah'])){
                $this->message = 'Pastikan semua item memiliki jumlah';
                $cek = TRUE;
                break;

            }
            if(!isset($item['sub_total'])){
                $this->message = 'Pastikan semua item memiliki sub total';
                $cek = TRUE;
                break;

            }
        }
        $status = $cek ? FALSE:TRUE;
        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}