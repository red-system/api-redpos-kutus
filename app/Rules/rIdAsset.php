<?php
namespace App\Rules;

use App\Models\mAcAsset;
use Illuminate\Contracts\Validation\ImplicitRule;

class rIdAsset implements ImplicitRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $id_asset = $value;

        $check = mAcAsset::where('id', $id_asset)->count();
        if($check > 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ID Asset tidak tersedia';
    }
}