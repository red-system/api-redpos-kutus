<?php

namespace App\Rules;

use App\Models\mAcMaster;
use App\Helpers\Main;
use Illuminate\Contracts\Validation\ImplicitRule;

class rMasterIdBukuBesar implements ImplicitRule
{
    protected $message;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $master_id = $value;

        if ($master_id == 'all') {
            $status = TRUE;
        } elseif(Main::is_json($master_id) === FALSE) {
            $this->message = 'Format ID Master tidak berupa JSON';
        } else {
            $master_id = json_decode($master_id, TRUE);
            foreach ($master_id as $row) {
                $check = mAcMaster::where('master_id', $row)->count();
                if ($check == 0) {
                    $this->message = 'ID Master pada JSON ada yang tidak tersedia';
                    $status = FALSE;
                    break;
                } else {
                    $status = TRUE;
                }
            }

        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}