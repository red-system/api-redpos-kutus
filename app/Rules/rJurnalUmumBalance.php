<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;
use Illuminate\Support\Facades\Hash;

class rJurnalUmumBalance implements ImplicitRule
{
    protected $trs_debet;

    public function __construct($trs_debet)
    {
        $this->trs_debet = $trs_debet;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $trs_kredit = $value;
        $trs_debet = $this->trs_debet;

        if(array_sum($trs_kredit) !== array_sum($trs_debet)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tidak Balance antara Total Debet dengan Total Kredit';
    }
}