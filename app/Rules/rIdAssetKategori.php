<?php
namespace App\Rules;

use App\Models\mAcAssetKategori;
use App\Models\mAcMaster;
use Illuminate\Contracts\Validation\ImplicitRule;

class rIdAssetKategori implements ImplicitRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $id_asset_kategori = $value;

        $check = mAcAssetKategori::where('id', $id_asset_kategori)->count();
        if($check > 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ID Kategori Asset tidak tersedia';
    }
}