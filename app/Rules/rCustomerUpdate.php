<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mCustomer;

class rCustomerUpdate implements ImplicitRule
{
    protected $customer_id;
    protected $email;

    public function __construct($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email_db = mCustomer::where('customer_id', $this->customer_id)->value('email');
        $email = $value;
        if ($email_db == $email) {
            $status = TRUE;
        } else {
            $check = mCustomer
                ::where('email', $email)
                ->whereNotIn('email', [$email_db])
                ->count();

            if($check > 0) {
                $this->email = $email;
                $status = FALSE;
            } else {
                $status = TRUE;
            }
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Email '.$this->email.' tidak tersedia';
    }
}