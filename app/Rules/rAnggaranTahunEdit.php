<?php
namespace App\Rules;

use App\Models\mAcAnggaran;
use App\Models\mAcMaster;
use Illuminate\Contracts\Validation\ImplicitRule;

class rAnggaranTahunEdit implements ImplicitRule
{

    protected $anggaran_id;

    public function __construct($anggaran_id)
    {
        $this->anggaran_id = $anggaran_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $anggaran_tahun = $value;
        $anggaran_tahun_edit = mAcAnggaran::where('anggaran_id', $this->anggaran_id)->value('anggaran_tahun');

        if($anggaran_tahun == $anggaran_tahun_edit) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tahun Anggaran tidak tersedia';
    }
}