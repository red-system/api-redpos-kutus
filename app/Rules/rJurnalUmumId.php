<?php
namespace App\Rules;

use App\Models\mAcJurnalUmum;
use Illuminate\Contracts\Validation\ImplicitRule;

class rJurnalUmumId implements ImplicitRule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $jurnal_umum_id = $value;
        $check = mAcJurnalUmum::where('jurnal_umum_id', $jurnal_umum_id)->count();
        if($check > 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ID Jurnal Umum tidak tersedia';
    }
}