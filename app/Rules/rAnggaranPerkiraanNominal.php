<?php

namespace App\Rules;

use App\Models\mAcAnggaran;
use App\Models\mAcMaster;
use Illuminate\Contracts\Validation\ImplicitRule;

class rAnggaranPerkiraanNominal implements ImplicitRule
{

    protected $master_id;
    protected $nominal;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $anggaran = $value;
        $master_id = [];
        $nominal = [];
        foreach ($anggaran as $row) {
            $check = mAcMaster::where('master_id', $row['master_id'])->count();
            if ($check == 0) {
                $master_id[] = $row['master_id'];
            }

            if(!is_numeric($row['nominal'])) {
                $nominal[] = $row['nominal'];
            }
        }

        $this->master_id = $master_id;
        $this->nominal = $nominal;
        if(count($master_id) == 0) {
            $status = TRUE;
        }

        if(count($nominal) == 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $master_id = 'ID Master '.implode(',', $this->master_id).' tidak tersedia';
        $nominal = ' & Ada nominal yang tidak berupa angka, seperti: '.implode(',',$this->nominal);

        $message = $master_id;
        $message .= count($this->nominal) > 0 ? $nominal : '';

        return $message;
    }
}