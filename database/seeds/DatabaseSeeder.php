<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produk')->insert([
            'produk_nama' => "'".Str::random(10)."'",
            'produk_jenis_id' => mt_rand(152, 155),
            'produk_kode' => "'".Str::random(10)."'",
            'produk_minimal_stock' => 0,
            'disc_persen' => 0,
            'disc_nominal' => 0,
        ]);
    }

}
