/*
Navicat MySQL Data Transfer

Source Server         : Local C
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : gtn_forstaff_new

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-08-09 10:29:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for absence
-- ----------------------------
DROP TABLE IF EXISTS `absence`;
CREATE TABLE `absence` (
  `absence_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `absence_date` datetime DEFAULT NULL,
  `present_hour` time DEFAULT NULL,
  `return_hour` time DEFAULT NULL,
  `absence_publish` enum('yes','no') DEFAULT NULL,
  `absence_import` enum('yes','no') DEFAULT 'no',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`absence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of absence
-- ----------------------------

-- ----------------------------
-- Table structure for absence_summary
-- ----------------------------
DROP TABLE IF EXISTS `absence_summary`;
CREATE TABLE `absence_summary` (
  `absence_summary_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `total_late` tinyint(4) DEFAULT NULL,
  `total_overbreak` tinyint(4) DEFAULT NULL,
  `total_skipping` tinyint(4) DEFAULT NULL,
  `total_overtime` tinyint(4) DEFAULT NULL,
  `total_leave` tinyint(4) DEFAULT NULL,
  `total_paid_leave` int(11) DEFAULT NULL,
  `total_unpaid_leave` int(11) DEFAULT NULL,
  `total_working_days` tinyint(4) DEFAULT NULL,
  `total_work` int(11) DEFAULT NULL,
  `total_not_work` int(11) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`absence_summary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of absence_summary
-- ----------------------------

-- ----------------------------
-- Table structure for announcement
-- ----------------------------
DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `announcement_title` varchar(255) DEFAULT NULL,
  `announcement_description` text DEFAULT NULL,
  `announcement_publish` enum('yes','no') DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of announcement
-- ----------------------------

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_title` varchar(255) DEFAULT NULL,
  `banner_description` text DEFAULT NULL,
  `banner_publish` enum('yes','no') DEFAULT 'no',
  `banner_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner
-- ----------------------------

-- ----------------------------
-- Table structure for benefit
-- ----------------------------
DROP TABLE IF EXISTS `benefit`;
CREATE TABLE `benefit` (
  `benefit_id` int(11) NOT NULL AUTO_INCREMENT,
  `benefit_title` varchar(255) DEFAULT NULL,
  `benefit_description` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`benefit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of benefit
-- ----------------------------

-- ----------------------------
-- Table structure for claim
-- ----------------------------
DROP TABLE IF EXISTS `claim`;
CREATE TABLE `claim` (
  `claim_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `claim_date` datetime DEFAULT NULL,
  `claim_reason` text DEFAULT NULL,
  `claim_nominal` int(11) DEFAULT NULL,
  `claim_status` enum('wait','confirm','reject') DEFAULT 'wait',
  `claim_filename` text DEFAULT NULL,
  `claim_wait_date` datetime DEFAULT NULL,
  `claim_confirm_date` datetime DEFAULT NULL,
  `claim_reject_date` datetime DEFAULT NULL,
  `claim_reject_reason` datetime DEFAULT NULL,
  `confirm_staff_id` int(11) DEFAULT NULL,
  `reject_staff_id` int(11) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of claim
-- ----------------------------

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `company_inisial` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_website` varchar(255) DEFAULT NULL,
  `company_description` varchar(255) DEFAULT NULL,
  `company_found_date` datetime DEFAULT NULL,
  `company_telp_1` varchar(255) DEFAULT NULL,
  `company_telp_2` varchar(255) DEFAULT NULL,
  `company_fax` varchar(255) DEFAULT NULL,
  `company_email_1` varchar(255) DEFAULT NULL,
  `company_email_2` varchar(255) DEFAULT NULL,
  `company_province` varchar(255) DEFAULT NULL,
  `company_district` varchar(255) DEFAULT NULL,
  `company_address` text DEFAULT NULL,
  `company_map_iframe` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of company
-- ----------------------------

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of config
-- ----------------------------

-- ----------------------------
-- Table structure for dayoff
-- ----------------------------
DROP TABLE IF EXISTS `dayoff`;
CREATE TABLE `dayoff` (
  `dayoff_id` int(11) NOT NULL AUTO_INCREMENT,
  `dayoff_title` varchar(255) DEFAULT NULL,
  `dayoff_description` text DEFAULT NULL,
  `dayoff_date` datetime DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`dayoff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dayoff
-- ----------------------------

-- ----------------------------
-- Table structure for job_description
-- ----------------------------
DROP TABLE IF EXISTS `job_description`;
CREATE TABLE `job_description` (
  `job_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobd_title` varchar(255) DEFAULT NULL,
  `jobd_description` varchar(255) DEFAULT NULL,
  `jobd_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`job_description_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of job_description
-- ----------------------------

-- ----------------------------
-- Table structure for kpi_indicator
-- ----------------------------
DROP TABLE IF EXISTS `kpi_indicator`;
CREATE TABLE `kpi_indicator` (
  `kpi_indicator_id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi_indicator_parent_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `indicator_name` varchar(255) DEFAULT NULL,
  `indicator_weight` double DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kpi_indicator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kpi_indicator
-- ----------------------------

-- ----------------------------
-- Table structure for kpi_result_category
-- ----------------------------
DROP TABLE IF EXISTS `kpi_result_category`;
CREATE TABLE `kpi_result_category` (
  `kpi_result_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi_rc_name` varchar(255) DEFAULT NULL,
  `kpi_rc_range_start` tinyint(4) DEFAULT NULL,
  `kpi_rc_range_end` tinyint(4) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kpi_result_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kpi_result_category
-- ----------------------------

-- ----------------------------
-- Table structure for kpi_staff
-- ----------------------------
DROP TABLE IF EXISTS `kpi_staff`;
CREATE TABLE `kpi_staff` (
  `kpi_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `kpis_date_start` date DEFAULT NULL,
  `kpis_date_end` date DEFAULT NULL,
  `kpis_status` enum('process','accepted','revise') DEFAULT 'process',
  `kpis_publish` enum('yes','no') DEFAULT 'no',
  `kpis_process_date` datetime DEFAULT NULL,
  `kpis_accepted_date` datetime DEFAULT NULL,
  `kpis_revise_date` datetime DEFAULT NULL,
  `kpis_revise_reason` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kpi_staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kpi_staff
-- ----------------------------

-- ----------------------------
-- Table structure for kpi_staff_result
-- ----------------------------
DROP TABLE IF EXISTS `kpi_staff_result`;
CREATE TABLE `kpi_staff_result` (
  `kpi_staff_result_id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi_staff_id` int(11) DEFAULT NULL,
  `kpisr_percent` double DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kpi_staff_result_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kpi_staff_result
-- ----------------------------

-- ----------------------------
-- Table structure for learning_development
-- ----------------------------
DROP TABLE IF EXISTS `learning_development`;
CREATE TABLE `learning_development` (
  `learning_development_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `learningd_date` datetime DEFAULT NULL,
  `learningd_reason` text DEFAULT NULL,
  `learningd_nominal` int(11) DEFAULT NULL,
  `learningd_status` enum('wait','confirm','reject') DEFAULT NULL,
  `learningd_filename` text DEFAULT NULL,
  `learningd_wait_date` datetime DEFAULT NULL,
  `learningd_confirm_date` datetime DEFAULT NULL,
  `learningd_reject_date` datetime DEFAULT NULL,
  `learningd_reject_reason` text DEFAULT NULL,
  `confirm_staff_id` text DEFAULT NULL,
  `reject_staff_id` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`learning_development_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of learning_development
-- ----------------------------

-- ----------------------------
-- Table structure for leave_massive
-- ----------------------------
DROP TABLE IF EXISTS `leave_massive`;
CREATE TABLE `leave_massive` (
  `leave_massive_id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_massive_date` datetime DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`leave_massive_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leave_massive
-- ----------------------------

-- ----------------------------
-- Table structure for leave_master
-- ----------------------------
DROP TABLE IF EXISTS `leave_master`;
CREATE TABLE `leave_master` (
  `leave_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `leavem_name` varchar(255) DEFAULT NULL,
  `leavem_regulation` enum('yes','no') DEFAULT 'no',
  `leavem_attachment` enum('yes','no') DEFAULT 'no',
  `leavem_quota_recomendation` enum('yes','no') DEFAULT 'no',
  `leavem_need_replace` enum('yes','no') DEFAULT 'no',
  `leavem_reduce_quota` enum('yes','no') DEFAULT 'no',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`leave_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leave_master
-- ----------------------------

-- ----------------------------
-- Table structure for leave_quota
-- ----------------------------
DROP TABLE IF EXISTS `leave_quota`;
CREATE TABLE `leave_quota` (
  `leave_quota_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `leave_quota_year` year(4) DEFAULT NULL,
  `leave_quota_annual` tinyint(4) DEFAULT NULL,
  `leave_quota_used` tinyint(4) DEFAULT NULL,
  `craeted_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`leave_quota_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leave_quota
-- ----------------------------

-- ----------------------------
-- Table structure for leave_staff
-- ----------------------------
DROP TABLE IF EXISTS `leave_staff`;
CREATE TABLE `leave_staff` (
  `leave_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_master_id` int(11) DEFAULT NULL,
  `leave_staff_date` datetime DEFAULT NULL,
  `leave_staff_category` enum('yes','no') DEFAULT 'no',
  `leave_staff_duration` tinyint(4) DEFAULT NULL,
  `leave_staff_reason` text DEFAULT NULL,
  `leave_staff_attachment` varchar(255) DEFAULT NULL,
  `substitution_staff_id` int(11) DEFAULT NULL,
  `substitution_date` datetime DEFAULT NULL,
  `leave_staff_status` enum('unpublish','publish','canceled','confirm','accepted','reject') DEFAULT 'unpublish',
  `leave_staff_unpublish_date` datetime DEFAULT NULL,
  `leave_staff_publish_date` datetime DEFAULT NULL,
  `leave_staff_confirm_date` datetime DEFAULT NULL,
  `leave_staff_canceled_date` datetime DEFAULT NULL,
  `leave_staff_accepted_date` datetime DEFAULT NULL,
  `leave_staff_reject_date` datetime DEFAULT NULL,
  `leave_staff_reject_reason` text DEFAULT NULL,
  `accepted_staff_id` int(11) DEFAULT NULL,
  `reject_staff_id` int(11) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`leave_staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leave_staff
-- ----------------------------

-- ----------------------------
-- Table structure for log_activity
-- ----------------------------
DROP TABLE IF EXISTS `log_activity`;
CREATE TABLE `log_activity` (
  `log_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `menu_label` varchar(255) DEFAULT NULL,
  `log_activity_title` varchar(255) DEFAULT NULL,
  `log_activity_description` varchar(255) DEFAULT NULL,
  `log_activity_type` enum('get','insert','update','delete') DEFAULT 'get',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_activity
-- ----------------------------

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) DEFAULT NULL,
  `news_description` text DEFAULT NULL,
  `news_publish` enum('yes','no') DEFAULT 'no',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for overtime
-- ----------------------------
DROP TABLE IF EXISTS `overtime`;
CREATE TABLE `overtime` (
  `overtime_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `overtime_date_request` date DEFAULT NULL,
  `overtime_reason` text DEFAULT NULL,
  `overtime_status` enum('unpublish','publish','approved','confirmed','canceled','rejected') DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `confirmed_date` datetime DEFAULT NULL,
  `canceled_date` datetime DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `unpublish_staff_id` int(11) DEFAULT NULL,
  `publish_staff_id` int(11) DEFAULT NULL,
  `approved_staff_id` int(11) DEFAULT NULL,
  `confirmed_staff_id` int(11) DEFAULT NULL,
  `canceled_staff_id` int(11) DEFAULT NULL,
  `rejected_staff_id` int(11) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `overtime_date_from` datetime DEFAULT NULL,
  `overtime_date_to` datetime DEFAULT NULL,
  PRIMARY KEY (`overtime_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of overtime
-- ----------------------------

-- ----------------------------
-- Table structure for punishment
-- ----------------------------
DROP TABLE IF EXISTS `punishment`;
CREATE TABLE `punishment` (
  `punishment_id` int(11) NOT NULL AUTO_INCREMENT,
  `punishment_guide_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `punishment_date` date DEFAULT NULL,
  `punishment_publish` enum('') DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`punishment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of punishment
-- ----------------------------

-- ----------------------------
-- Table structure for punishment_guide
-- ----------------------------
DROP TABLE IF EXISTS `punishment_guide`;
CREATE TABLE `punishment_guide` (
  `punishment_guide_id` int(11) NOT NULL AUTO_INCREMENT,
  `punishmentg_title` varchar(255) DEFAULT NULL,
  `punishmentg_description` text DEFAULT NULL,
  `punishmentg_publish` enum('yes','no') DEFAULT 'no',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`punishment_guide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of punishment_guide
-- ----------------------------

-- ----------------------------
-- Table structure for resign
-- ----------------------------
DROP TABLE IF EXISTS `resign`;
CREATE TABLE `resign` (
  `resign_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `resign_date` datetime DEFAULT NULL,
  `resign_reason` text DEFAULT NULL,
  `resign_status` enum('process','accept','reject') DEFAULT 'process',
  `resign_process_date` datetime DEFAULT NULL,
  `resign_accept_date` datetime DEFAULT NULL,
  `resign_accept_reason` text DEFAULT NULL,
  `resign_reject_date` datetime DEFAULT NULL,
  `resign_reject_reason` text DEFAULT NULL,
  `accept_staff_id` int(11) DEFAULT NULL,
  `reject_staff_id` int(11) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`resign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of resign
-- ----------------------------

-- ----------------------------
-- Table structure for reward
-- ----------------------------
DROP TABLE IF EXISTS `reward`;
CREATE TABLE `reward` (
  `reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_guide_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `reward_publish` enum('yes','no') DEFAULT 'no',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reward
-- ----------------------------

-- ----------------------------
-- Table structure for reward_guide
-- ----------------------------
DROP TABLE IF EXISTS `reward_guide`;
CREATE TABLE `reward_guide` (
  `reward_guide_id` int(11) NOT NULL AUTO_INCREMENT,
  `rewardg_title` varchar(255) DEFAULT '',
  `rewardg_description` text DEFAULT NULL,
  `rewardg_publish` enum('yes','no') DEFAULT 'yes',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_guide_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reward_guide
-- ----------------------------
INSERT INTO `reward_guide` VALUES ('1', 'Title', 'reward desc', 'yes', '1', '2019-08-27 16:41:24', '1', '2019-08-07 16:41:33');
INSERT INTO `reward_guide` VALUES ('2', 'Title 2', 'reward desc 2', 'yes', '2', '2019-08-20 16:41:55', '2', '2019-08-05 16:42:01');

-- ----------------------------
-- Table structure for role_master
-- ----------------------------
DROP TABLE IF EXISTS `role_master`;
CREATE TABLE `role_master` (
  `role_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_master_title` varchar(255) DEFAULT NULL,
  `role_master_description` text DEFAULT NULL,
  `role_master_list` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_master
-- ----------------------------

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `role_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `role_master_id_json` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`role_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_user
-- ----------------------------

-- ----------------------------
-- Table structure for rules_regulation
-- ----------------------------
DROP TABLE IF EXISTS `rules_regulation`;
CREATE TABLE `rules_regulation` (
  `rules_regulation_id` int(11) NOT NULL AUTO_INCREMENT,
  `rulesr_title` varchar(255) DEFAULT NULL,
  `rulesr_description` text DEFAULT NULL,
  `rulesr_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`rules_regulation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rules_regulation
-- ----------------------------

-- ----------------------------
-- Table structure for sallary
-- ----------------------------
DROP TABLE IF EXISTS `sallary`;
CREATE TABLE `sallary` (
  `sallary_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `sallary_year` varchar(255) DEFAULT NULL,
  `sallary_month` varchar(255) DEFAULT NULL,
  `sallary_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sallary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sallary
-- ----------------------------

-- ----------------------------
-- Table structure for shift_master
-- ----------------------------
DROP TABLE IF EXISTS `shift_master`;
CREATE TABLE `shift_master` (
  `shift_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `shiftm_present_hour` time DEFAULT NULL,
  `shiftm_return_hour` time DEFAULT NULL,
  `shiftm_tolerance_minutes` tinyint(4) DEFAULT NULL,
  `shiftm_description` text DEFAULT NULL,
  `shiftm_total_work_hour` tinyint(4) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`shift_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shift_master
-- ----------------------------

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `structure_id` int(11) DEFAULT NULL,
  `roles_id` text DEFAULT NULL,
  `staff_token` varchar(255) DEFAULT '',
  `staff_id_label` varchar(255) DEFAULT NULL,
  `staff_name` varchar(255) DEFAULT NULL,
  `staff_work_start` datetime DEFAULT NULL,
  `staff_quota_leave` tinyint(4) DEFAULT NULL,
  `staff_username` varchar(100) DEFAULT NULL,
  `staff_password` varchar(255) DEFAULT '',
  `staff_status` enum('active','not_active') DEFAULT 'active',
  `staff_nationality` enum('wna','wni') DEFAULT 'wna',
  `staff_gender` enum('male','female') DEFAULT NULL,
  `staff_birth_place` varchar(255) DEFAULT NULL,
  `staff_birth_date` datetime DEFAULT NULL,
  `staff_identity_type` enum('ktp','sim','stnk') DEFAULT NULL,
  `staff_identity_number` varchar(255) DEFAULT NULL,
  `staff_religion` varchar(255) DEFAULT NULL,
  `staff_marital_status` enum('yes','no') DEFAULT 'no',
  `staff_primary_address` varchar(255) DEFAULT NULL,
  `staff_primary_province` varchar(255) DEFAULT NULL,
  `staff_primary_district` varchar(255) DEFAULT NULL,
  `staff_primary_post_code` varchar(255) DEFAULT NULL,
  `staff_second_address` varchar(255) DEFAULT NULL,
  `staff_second_province` varchar(255) DEFAULT NULL,
  `staff_second_district` varchar(255) DEFAULT NULL,
  `staff_second_post_code` varchar(255) DEFAULT NULL,
  `staff_phone_number` varchar(255) DEFAULT NULL,
  `staff_telephone_number` varchar(255) DEFAULT NULL,
  `staff_email` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', '1', '1', '7ee9179777c3ee89e3c9b43f5b77906a36e39269b1623b2079f4388cc5cee7f1d9925ebc6158d4c1e0087838b90e536e240bcc12de06ca66428ba5ca884a8ec1', '', 'Admin', '0000-00-00 00:00:00', '0', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00');
INSERT INTO `staff` VALUES ('2', '1', '1', '1172d203d5fdd2c883905b7cedef6d9205272ace6c09a80ad0a8dbb6e0f284819a8d5aa2a622acbbd4e279a9a5afad8617315d19c989e5db4811bfecf6651ac7', null, 'Admin 1', null, null, 'admin1', '$2y$12$9p3ZqqNMh6nsM1gwwBgzseGQB9zlusZQIlh.kanncwnbF18Qm9Atq', 'active', 'wna', null, null, null, null, null, null, 'no', null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2019-08-09 09:06:46');

-- ----------------------------
-- Table structure for staff_benefit
-- ----------------------------
DROP TABLE IF EXISTS `staff_benefit`;
CREATE TABLE `staff_benefit` (
  `staff_benefit_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `benefit_id_json` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_benefit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_benefit
-- ----------------------------

-- ----------------------------
-- Table structure for staff_certificate
-- ----------------------------
DROP TABLE IF EXISTS `staff_certificate`;
CREATE TABLE `staff_certificate` (
  `staff_certificate_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_certificate_skill` varchar(255) DEFAULT NULL,
  `staff_certificate_date_from` datetime DEFAULT NULL,
  `staff_certificate_date_to` datetime DEFAULT NULL,
  `staff_certificate_level` varchar(255) DEFAULT NULL,
  `staff_certificate_description` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_certificate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_certificate
-- ----------------------------

-- ----------------------------
-- Table structure for staff_contract
-- ----------------------------
DROP TABLE IF EXISTS `staff_contract`;
CREATE TABLE `staff_contract` (
  `staff_contract_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staffc_contract_date_from` date DEFAULT NULL,
  `staffc_contract_date_to` date DEFAULT NULL,
  `staffc_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_contract
-- ----------------------------

-- ----------------------------
-- Table structure for staff_document
-- ----------------------------
DROP TABLE IF EXISTS `staff_document`;
CREATE TABLE `staff_document` (
  `staff_document_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_document_title` varchar(255) DEFAULT NULL,
  `staff_document_type` varchar(255) DEFAULT NULL,
  `staff_document_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_document
-- ----------------------------

-- ----------------------------
-- Table structure for staff_education
-- ----------------------------
DROP TABLE IF EXISTS `staff_education`;
CREATE TABLE `staff_education` (
  `staff_education_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_education_date_from` date DEFAULT NULL,
  `staff_education_date_to` date DEFAULT NULL,
  `staff_education_place` varchar(255) DEFAULT NULL,
  `staff_education_department` varchar(255) DEFAULT NULL,
  `staff_education_qualified` double DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_education_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_education
-- ----------------------------

-- ----------------------------
-- Table structure for staff_family
-- ----------------------------
DROP TABLE IF EXISTS `staff_family`;
CREATE TABLE `staff_family` (
  `staff_family_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_family_name` varchar(255) DEFAULT NULL,
  `staff_family_relation` varchar(255) DEFAULT NULL,
  `staff_family_birthday` date DEFAULT NULL,
  `staff_family_contact` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_family_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_family
-- ----------------------------

-- ----------------------------
-- Table structure for staff_job_description
-- ----------------------------
DROP TABLE IF EXISTS `staff_job_description`;
CREATE TABLE `staff_job_description` (
  `staff_job_desc_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_job_desc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_job_description
-- ----------------------------

-- ----------------------------
-- Table structure for staff_job_history
-- ----------------------------
DROP TABLE IF EXISTS `staff_job_history`;
CREATE TABLE `staff_job_history` (
  `staff_job_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `staff_jh_company` varchar(255) DEFAULT NULL,
  `staff_jh_date_from` datetime DEFAULT NULL,
  `staff_jh_date_to` datetime DEFAULT NULL,
  `staff_jh_address` varchar(255) DEFAULT NULL,
  `staff_jh_telp` varchar(255) DEFAULT NULL,
  `staff_jh_bussines_field` varchar(255) DEFAULT NULL,
  `staff_jh_boss` varchar(255) DEFAULT NULL,
  `staff_jh_job_description` text DEFAULT NULL,
  `staff_jh_position` varchar(255) DEFAULT NULL,
  `staff_jh_sallary` int(11) DEFAULT NULL,
  `staff_jh_reason_quit` text DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_job_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_job_history
-- ----------------------------

-- ----------------------------
-- Table structure for staff_structure_history
-- ----------------------------
DROP TABLE IF EXISTS `staff_structure_history`;
CREATE TABLE `staff_structure_history` (
  `staff_structure_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`staff_structure_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff_structure_history
-- ----------------------------

-- ----------------------------
-- Table structure for structure
-- ----------------------------
DROP TABLE IF EXISTS `structure`;
CREATE TABLE `structure` (
  `structure_id` int(11) NOT NULL AUTO_INCREMENT,
  `structure_parent_id` varchar(255) DEFAULT '',
  `structure_name` varchar(255) DEFAULT '',
  `structure_description` varchar(255) DEFAULT '',
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`structure_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of structure
-- ----------------------------
INSERT INTO `structure` VALUES ('2', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:03:17', null, '2019-08-09 00:03:17');
INSERT INTO `structure` VALUES ('3', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:08:26', null, '2019-08-09 00:08:26');
INSERT INTO `structure` VALUES ('4', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:08:29', null, '2019-08-09 00:08:29');
INSERT INTO `structure` VALUES ('5', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:36:43', null, '2019-08-09 00:36:43');
INSERT INTO `structure` VALUES ('6', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:37:19', null, '2019-08-09 00:37:19');
INSERT INTO `structure` VALUES ('7', '0', 'CTO', null, null, '2019-08-09 00:37:25', null, '2019-08-09 00:37:25');
INSERT INTO `structure` VALUES ('8', '0', null, null, null, '2019-08-09 00:37:30', null, '2019-08-09 00:37:30');
INSERT INTO `structure` VALUES ('9', null, null, null, null, '2019-08-09 00:37:33', null, '2019-08-09 00:37:33');
INSERT INTO `structure` VALUES ('10', null, null, null, null, '2019-08-09 00:38:12', null, '2019-08-09 00:38:12');
INSERT INTO `structure` VALUES ('11', null, null, null, null, '2019-08-09 00:41:19', null, '2019-08-09 00:41:19');
INSERT INTO `structure` VALUES ('12', null, null, null, null, '2019-08-09 00:41:31', null, '2019-08-09 00:41:31');
INSERT INTO `structure` VALUES ('13', null, null, null, null, '2019-08-09 00:41:32', null, '2019-08-09 00:41:32');
INSERT INTO `structure` VALUES ('14', null, null, null, null, '2019-08-09 00:41:49', null, '2019-08-09 00:41:49');
INSERT INTO `structure` VALUES ('15', null, null, null, null, '2019-08-09 00:41:51', null, '2019-08-09 00:41:51');
INSERT INTO `structure` VALUES ('16', null, null, null, null, '2019-08-09 00:41:52', null, '2019-08-09 00:41:52');
INSERT INTO `structure` VALUES ('17', null, null, null, null, '2019-08-09 00:41:53', null, '2019-08-09 00:41:53');
INSERT INTO `structure` VALUES ('18', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:43:11', null, '2019-08-09 00:43:11');
INSERT INTO `structure` VALUES ('19', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:43:20', null, '2019-08-09 00:43:20');
INSERT INTO `structure` VALUES ('20', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:43:22', null, '2019-08-09 00:43:22');
INSERT INTO `structure` VALUES ('21', '0', 'CTO', 'Technology Leader', null, '2019-08-09 00:43:58', null, '2019-08-09 00:43:58');
INSERT INTO `structure` VALUES ('22', '0', 'CTO', 'Technology Leader', null, '2019-08-09 01:00:58', null, '2019-08-09 01:00:58');
INSERT INTO `structure` VALUES ('23', '0', 'CTO', 'Technology Leader', null, '2019-08-09 01:00:59', null, '2019-08-09 01:00:59');
INSERT INTO `structure` VALUES ('24', '0', 'CTO', 'Technology Leader', null, '2019-08-09 09:03:26', null, '2019-08-09 09:03:26');

-- ----------------------------
-- Table structure for workdays
-- ----------------------------
DROP TABLE IF EXISTS `workdays`;
CREATE TABLE `workdays` (
  `workdays_id` int(11) NOT NULL AUTO_INCREMENT,
  `days` varchar(255) DEFAULT NULL,
  `present_hour` time DEFAULT NULL,
  `return_hour` time DEFAULT NULL,
  `tolerance_minutes` tinyint(4) DEFAULT NULL,
  `total_work_hour` tinyint(4) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`workdays_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of workdays
-- ----------------------------

-- ----------------------------
-- Table structure for work_flow
-- ----------------------------
DROP TABLE IF EXISTS `work_flow`;
CREATE TABLE `work_flow` (
  `work_flow_id` int(11) NOT NULL AUTO_INCREMENT,
  `workf_title` varchar(255) DEFAULT NULL,
  `workf_description` varchar(255) DEFAULT NULL,
  `workf_filename` varchar(255) DEFAULT NULL,
  `created_staff_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_staff_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`work_flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of work_flow
-- ----------------------------
